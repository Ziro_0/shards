<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>5</int>
        <key>texturePackerVersion</key>
        <string>6.0.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../src/assets/game-shards/shards.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">BALL_FAST.png</key>
            <key type="filename">BALL_NORMAL.png</key>
            <key type="filename">BALL_POWER.png</key>
            <key type="filename">BALL_SLOW.png</key>
            <key type="filename">BALL_STICKY.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_PTC_FAST.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,16,37,32</rect>
                <key>scale9Paddings</key>
                <rect>19,16,37,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_PTC_NORMAL.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,19,37,37</rect>
                <key>scale9Paddings</key>
                <rect>19,19,37,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_PTC_POWER.png</key>
            <key type="filename">TROPHY_GLOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_PTC_SLOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,15,35,31</rect>
                <key>scale9Paddings</key>
                <rect>18,15,35,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_PTC_STICKY.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,12,28,25</rect>
                <key>scale9Paddings</key>
                <rect>14,12,28,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BALL_SHADOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,17,17</rect>
                <key>scale9Paddings</key>
                <rect>8,8,17,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_0.png</key>
            <key type="filename">BONUS_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,31,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,31,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,31</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_1UP.png</key>
            <key type="filename">BONUS_SHADOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,32,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,32,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_3.png</key>
            <key type="filename">BONUS_4.png</key>
            <key type="filename">BONUS_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,16,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_BOMB.png</key>
            <key type="filename">BONUS_STICKY.png</key>
            <key type="filename">BONUS_STICKY_1.png</key>
            <key type="filename">PWR_all_shadow_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,23,23</rect>
                <key>scale9Paddings</key>
                <rect>12,12,23,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_BOMB_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,21,21</rect>
                <key>scale9Paddings</key>
                <rect>11,11,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_BOMB_SHADOW.png</key>
            <key type="filename">PWR_all_shadow_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,17</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_CANNON.png</key>
            <key type="filename">BONUS_CANNON_1.png</key>
            <key type="filename">PWR_all_shadow_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,20,23</rect>
                <key>scale9Paddings</key>
                <rect>10,11,20,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_ENLARGE.png</key>
            <key type="filename">BONUS_ENLARGE_1.png</key>
            <key type="filename">PWR_all_shadow_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,10,23,19</rect>
                <key>scale9Paddings</key>
                <rect>12,10,23,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_FAST.png</key>
            <key type="filename">BONUS_FAST_1.png</key>
            <key type="filename">PWR_all_shadow_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,13,21,25</rect>
                <key>scale9Paddings</key>
                <rect>10,13,21,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_POWER.png</key>
            <key type="filename">BONUS_POWER_1.png</key>
            <key type="filename">PWR_all_shadow_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,11,21,21</rect>
                <key>scale9Paddings</key>
                <rect>10,11,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_SHRINK.png</key>
            <key type="filename">BONUS_SHRINK_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,19</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_SLOW.png</key>
            <key type="filename">BONUS_SLOW_1.png</key>
            <key type="filename">PWR_all_shadow_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,10,21,19</rect>
                <key>scale9Paddings</key>
                <rect>11,10,21,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_TIME.png</key>
            <key type="filename">BONUS_TIME_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,22,22</rect>
                <key>scale9Paddings</key>
                <rect>11,11,22,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BONUS_TRIPLE.png</key>
            <key type="filename">BONUS_TRIPLE_1.png</key>
            <key type="filename">PWR_all_shadow_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,21,22</rect>
                <key>scale9Paddings</key>
                <rect>11,11,21,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BORDER_HORIZONTAL.png</key>
            <key type="filename">BORDER_HORIZONTAL2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,4,47,9</rect>
                <key>scale9Paddings</key>
                <rect>23,4,47,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BORDER_TOP_LEFT.png</key>
            <key type="filename">BORDER_TOP_RIGHT.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,15,15</rect>
                <key>scale9Paddings</key>
                <rect>7,7,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BORDER_VERTICAL_LEFT.png</key>
            <key type="filename">BORDER_VERTICAL_LEFT2.png</key>
            <key type="filename">BORDER_VERTICAL_RIGHT.png</key>
            <key type="filename">BORDER_VERTICAL_RIGHT2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,25,9,50</rect>
                <key>scale9Paddings</key>
                <rect>4,25,9,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHADOW.png</key>
            <key type="filename">Brick_Shadow_1.png</key>
            <key type="filename">Brick_Shadow_2.png</key>
            <key type="filename">Brick_Shadow_3.png</key>
            <key type="filename">Brick_Shadow_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,9,38,18</rect>
                <key>scale9Paddings</key>
                <rect>19,9,38,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,13,17</rect>
                <key>scale9Paddings</key>
                <rect>6,8,13,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,9,21,18</rect>
                <key>scale9Paddings</key>
                <rect>10,9,21,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,9,29,18</rect>
                <key>scale9Paddings</key>
                <rect>15,9,29,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,9,36,18</rect>
                <key>scale9Paddings</key>
                <rect>18,9,36,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,9,33,18</rect>
                <key>scale9Paddings</key>
                <rect>17,9,33,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">BRICK_SHINE_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,9,25,18</rect>
                <key>scale9Paddings</key>
                <rect>12,9,25,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Brick_Indestructible_1.png</key>
            <key type="filename">Brick_Indestructible_10.png</key>
            <key type="filename">Brick_Indestructible_2.png</key>
            <key type="filename">Brick_Indestructible_3.png</key>
            <key type="filename">Brick_Indestructible_4.png</key>
            <key type="filename">Brick_Indestructible_5.png</key>
            <key type="filename">Brick_Indestructible_6.png</key>
            <key type="filename">Brick_Indestructible_7.png</key>
            <key type="filename">Brick_Indestructible_8.png</key>
            <key type="filename">Brick_Indestructible_9.png</key>
            <key type="filename">Brick_Normal_2.png</key>
            <key type="filename">Brick_Normal_3.png</key>
            <key type="filename">Brick_Normal_4.png</key>
            <key type="filename">brick_black.png</key>
            <key type="filename">brick_black1.png</key>
            <key type="filename">brick_black2.png</key>
            <key type="filename">brick_black3.png</key>
            <key type="filename">brick_black4.png</key>
            <key type="filename">brick_blue.png</key>
            <key type="filename">brick_blue1.png</key>
            <key type="filename">brick_blue2.png</key>
            <key type="filename">brick_blue3.png</key>
            <key type="filename">brick_blue4.png</key>
            <key type="filename">brick_brown.png</key>
            <key type="filename">brick_brown1.png</key>
            <key type="filename">brick_brown2.png</key>
            <key type="filename">brick_brown3.png</key>
            <key type="filename">brick_brown4.png</key>
            <key type="filename">brick_cyan.png</key>
            <key type="filename">brick_cyan1.png</key>
            <key type="filename">brick_cyan2.png</key>
            <key type="filename">brick_cyan3.png</key>
            <key type="filename">brick_cyan4.png</key>
            <key type="filename">brick_green.png</key>
            <key type="filename">brick_green1.png</key>
            <key type="filename">brick_green2.png</key>
            <key type="filename">brick_green3.png</key>
            <key type="filename">brick_green4.png</key>
            <key type="filename">brick_green_yellow.png</key>
            <key type="filename">brick_green_yellow1.png</key>
            <key type="filename">brick_green_yellow2.png</key>
            <key type="filename">brick_green_yellow3.png</key>
            <key type="filename">brick_green_yellow4.png</key>
            <key type="filename">brick_hit.png</key>
            <key type="filename">brick_lemonade.png</key>
            <key type="filename">brick_lemonade1.png</key>
            <key type="filename">brick_lemonade2.png</key>
            <key type="filename">brick_lemonade3.png</key>
            <key type="filename">brick_lemonade4.png</key>
            <key type="filename">brick_magenta.png</key>
            <key type="filename">brick_magenta1.png</key>
            <key type="filename">brick_magenta2.png</key>
            <key type="filename">brick_magenta3.png</key>
            <key type="filename">brick_magenta4.png</key>
            <key type="filename">brick_orange.png</key>
            <key type="filename">brick_orange1.png</key>
            <key type="filename">brick_orange2.png</key>
            <key type="filename">brick_orange3.png</key>
            <key type="filename">brick_orange4.png</key>
            <key type="filename">brick_pale.png</key>
            <key type="filename">brick_pale1.png</key>
            <key type="filename">brick_pale2.png</key>
            <key type="filename">brick_pale3.png</key>
            <key type="filename">brick_pale4.png</key>
            <key type="filename">brick_pale_yellow.png</key>
            <key type="filename">brick_pale_yellow1.png</key>
            <key type="filename">brick_pale_yellow2.png</key>
            <key type="filename">brick_pale_yellow3.png</key>
            <key type="filename">brick_pale_yellow4.png</key>
            <key type="filename">brick_red.png</key>
            <key type="filename">brick_red1.png</key>
            <key type="filename">brick_red2.png</key>
            <key type="filename">brick_red3.png</key>
            <key type="filename">brick_red4.png</key>
            <key type="filename">brick_silver.png</key>
            <key type="filename">brick_silver1.png</key>
            <key type="filename">brick_silver2.png</key>
            <key type="filename">brick_silver3.png</key>
            <key type="filename">brick_silver4.png</key>
            <key type="filename">brick_tan.png</key>
            <key type="filename">brick_tan1.png</key>
            <key type="filename">brick_tan2.png</key>
            <key type="filename">brick_tan3.png</key>
            <key type="filename">brick_tan4.png</key>
            <key type="filename">brick_white.png</key>
            <key type="filename">brick_white1.png</key>
            <key type="filename">brick_white2.png</key>
            <key type="filename">brick_white3.png</key>
            <key type="filename">brick_white4.png</key>
            <key type="filename">brick_yellow.png</key>
            <key type="filename">brick_yellow1.png</key>
            <key type="filename">brick_yellow2.png</key>
            <key type="filename">brick_yellow3.png</key>
            <key type="filename">brick_yellow4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,12,38,24</rect>
                <key>scale9Paddings</key>
                <rect>19,12,38,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ENEMY_BLOCKER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,21,43,42</rect>
                <key>scale9Paddings</key>
                <rect>21,21,43,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ENEMY_HIVE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,49,49</rect>
                <key>scale9Paddings</key>
                <rect>25,25,49,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ENEMY_PASSER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,22,43,43</rect>
                <key>scale9Paddings</key>
                <rect>22,22,43,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ENEMY_SHADOW_BLOCKER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,45,45</rect>
                <key>scale9Paddings</key>
                <rect>23,23,45,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">ENEMY_SHADOW_PASSER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,24,45,49</rect>
                <key>scale9Paddings</key>
                <rect>23,24,45,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,19,37,38</rect>
                <key>scale9Paddings</key>
                <rect>18,19,37,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,21,39,42</rect>
                <key>scale9Paddings</key>
                <rect>20,21,39,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,23,43,45</rect>
                <key>scale9Paddings</key>
                <rect>21,23,43,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,23,44,47</rect>
                <key>scale9Paddings</key>
                <rect>22,23,44,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,22,47,44</rect>
                <key>scale9Paddings</key>
                <rect>24,22,47,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,25,52,49</rect>
                <key>scale9Paddings</key>
                <rect>26,25,52,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,24,46,47</rect>
                <key>scale9Paddings</key>
                <rect>23,24,46,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,23,43,45</rect>
                <key>scale9Paddings</key>
                <rect>22,23,43,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,19,43,37</rect>
                <key>scale9Paddings</key>
                <rect>22,19,43,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">EXPLOSION_SHARDS.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,56,147,112</rect>
                <key>scale9Paddings</key>
                <rect>74,56,147,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GLOW.png</key>
            <key type="filename">PWR_all_shadow_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,19,19</rect>
                <key>scale9Paddings</key>
                <rect>10,10,19,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_0.png</key>
            <key type="filename">GUN_1.png</key>
            <key type="filename">GUN_10.png</key>
            <key type="filename">GUN_11.png</key>
            <key type="filename">GUN_12.png</key>
            <key type="filename">GUN_13.png</key>
            <key type="filename">GUN_14.png</key>
            <key type="filename">GUN_15.png</key>
            <key type="filename">GUN_2.png</key>
            <key type="filename">GUN_3.png</key>
            <key type="filename">GUN_4.png</key>
            <key type="filename">GUN_5.png</key>
            <key type="filename">GUN_6.png</key>
            <key type="filename">GUN_7.png</key>
            <key type="filename">GUN_8.png</key>
            <key type="filename">GUN_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,9,17,17</rect>
                <key>scale9Paddings</key>
                <rect>9,9,17,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_16.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,10,17,21</rect>
                <key>scale9Paddings</key>
                <rect>9,10,17,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,13,17,25</rect>
                <key>scale9Paddings</key>
                <rect>9,13,17,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_18.png</key>
            <key type="filename">GUN_19.png</key>
            <key type="filename">GUN_20.png</key>
            <key type="filename">GUN_21.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,15,17,29</rect>
                <key>scale9Paddings</key>
                <rect>9,15,17,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_0.png</key>
            <key type="filename">Shards_17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_1.png</key>
            <key type="filename">GUN_EXPLO_6.png</key>
            <key type="filename">GUN_EXPLO_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_11.png</key>
            <key type="filename">GUN_EXPLO_12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,13</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_13.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,15,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_2.png</key>
            <key type="filename">GUN_EXPLO_3.png</key>
            <key type="filename">GUN_EXPLO_4.png</key>
            <key type="filename">GUN_EXPLO_5.png</key>
            <key type="filename">HIT_PARTICLE_ICE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">GUN_EXPLO_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">HIT_PARTICLE_FIRE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,16,11</rect>
                <key>scale9Paddings</key>
                <rect>8,6,16,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">LASER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,27,21,53</rect>
                <key>scale9Paddings</key>
                <rect>11,27,21,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">LASER_SHADOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,23,19,45</rect>
                <key>scale9Paddings</key>
                <rect>10,23,19,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,9,19</rect>
                <key>scale9Paddings</key>
                <rect>5,9,9,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,13,7,27</rect>
                <key>scale9Paddings</key>
                <rect>4,13,7,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,13,6,26</rect>
                <key>scale9Paddings</key>
                <rect>3,13,6,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,10,6,19</rect>
                <key>scale9Paddings</key>
                <rect>3,10,6,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">MUZZLE_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,7,6,13</rect>
                <key>scale9Paddings</key>
                <rect>3,7,6,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE.png</key>
            <key type="filename">PADDLE_RED_FLASH.png</key>
            <key type="filename">PADDLE_WHITE_FLASH.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,16,83,31</rect>
                <key>scale9Paddings</key>
                <rect>41,16,83,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE_FIRE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,20,91,40</rect>
                <key>scale9Paddings</key>
                <rect>46,20,91,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE_FROST.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,18,84,35</rect>
                <key>scale9Paddings</key>
                <rect>42,18,84,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE_POWER.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,19,89,38</rect>
                <key>scale9Paddings</key>
                <rect>45,19,89,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE_SHADOW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,14,79,28</rect>
                <key>scale9Paddings</key>
                <rect>39,14,79,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PADDLE_STICKY.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,17,83,35</rect>
                <key>scale9Paddings</key>
                <rect>41,17,83,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">PAUSE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,10,19,20</rect>
                <key>scale9Paddings</key>
                <rect>9,10,19,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">SHOCKWAVE.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,45,46</rect>
                <key>scale9Paddings</key>
                <rect>23,23,45,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">START_ARROW.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,6,25,13</rect>
                <key>scale9Paddings</key>
                <rect>12,6,25,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,2,15,3</rect>
                <key>scale9Paddings</key>
                <rect>8,2,15,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_1.png</key>
            <key type="filename">Shards_27.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,2,15,4</rect>
                <key>scale9Paddings</key>
                <rect>8,2,15,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,11</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_11.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,7,9,15</rect>
                <key>scale9Paddings</key>
                <rect>5,7,9,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,8,7,15</rect>
                <key>scale9Paddings</key>
                <rect>4,8,7,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_13.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,7,10,15</rect>
                <key>scale9Paddings</key>
                <rect>5,7,10,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_14.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,13,11</rect>
                <key>scale9Paddings</key>
                <rect>7,6,13,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_15.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,15,11</rect>
                <key>scale9Paddings</key>
                <rect>8,6,15,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_16.png</key>
            <key type="filename">Shards_18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,15,13</rect>
                <key>scale9Paddings</key>
                <rect>8,6,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,4,15,9</rect>
                <key>scale9Paddings</key>
                <rect>8,4,15,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,4,15,7</rect>
                <key>scale9Paddings</key>
                <rect>8,4,15,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_20.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,14,9</rect>
                <key>scale9Paddings</key>
                <rect>7,4,14,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_21.png</key>
            <key type="filename">Shards_31.png</key>
            <key type="filename">Shards_32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>7,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,13,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_23.png</key>
            <key type="filename">Shards_33.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,13,8</rect>
                <key>scale9Paddings</key>
                <rect>7,4,13,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_24.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,14,7</rect>
                <key>scale9Paddings</key>
                <rect>7,4,14,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_25.png</key>
            <key type="filename">Shards_40.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,15,6</rect>
                <key>scale9Paddings</key>
                <rect>7,3,15,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_26.png</key>
            <key type="filename">Shards_36.png</key>
            <key type="filename">Shards_37.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,2,15,5</rect>
                <key>scale9Paddings</key>
                <rect>8,2,15,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_28.png</key>
            <key type="filename">Shards_35.png</key>
            <key type="filename">Shards_38.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,3,15,5</rect>
                <key>scale9Paddings</key>
                <rect>8,3,15,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_29.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,15,7</rect>
                <key>scale9Paddings</key>
                <rect>7,4,15,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,15,10</rect>
                <key>scale9Paddings</key>
                <rect>7,5,15,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_30.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,14,8</rect>
                <key>scale9Paddings</key>
                <rect>7,4,14,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_34.png</key>
            <key type="filename">Shards_41.png</key>
            <key type="filename">Shards_48.png</key>
            <key type="filename">Shards_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,13,7</rect>
                <key>scale9Paddings</key>
                <rect>7,4,13,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_39.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,15,7</rect>
                <key>scale9Paddings</key>
                <rect>7,3,15,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,12,10</rect>
                <key>scale9Paddings</key>
                <rect>6,5,12,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_42.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_43.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_44.png</key>
            <key type="filename">Shards_7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_45.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,11,8</rect>
                <key>scale9Paddings</key>
                <rect>5,4,11,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_46.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,13,7</rect>
                <key>scale9Paddings</key>
                <rect>6,4,13,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_47.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,13,7</rect>
                <key>scale9Paddings</key>
                <rect>7,3,13,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_49.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,13,9</rect>
                <key>scale9Paddings</key>
                <rect>6,4,13,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_5.png</key>
            <key type="filename">Shards_51.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,9,11</rect>
                <key>scale9Paddings</key>
                <rect>5,6,9,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,11,9</rect>
                <key>scale9Paddings</key>
                <rect>6,5,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_52.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,7,12</rect>
                <key>scale9Paddings</key>
                <rect>3,6,7,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_53.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,6,12</rect>
                <key>scale9Paddings</key>
                <rect>3,6,6,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_54.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,7,11</rect>
                <key>scale9Paddings</key>
                <rect>4,5,7,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_55.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,11,9</rect>
                <key>scale9Paddings</key>
                <rect>6,4,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_56.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,13,5</rect>
                <key>scale9Paddings</key>
                <rect>7,3,13,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_57.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,13,3</rect>
                <key>scale9Paddings</key>
                <rect>7,2,13,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_58.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,14,3</rect>
                <key>scale9Paddings</key>
                <rect>7,2,14,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_59.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,14,5</rect>
                <key>scale9Paddings</key>
                <rect>7,2,14,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,6,7,11</rect>
                <key>scale9Paddings</key>
                <rect>4,6,7,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">Shards_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,15,9</rect>
                <key>scale9Paddings</key>
                <rect>7,5,15,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">TROPHY.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
