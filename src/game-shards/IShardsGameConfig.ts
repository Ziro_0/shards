import { getBooleanProperty, getIntProperty, getNumberProperty, getProperty } from './util/util';

/**
 * Once it's been decided that an item _will_ drop, this determines the weighted chance of
 * dropping an item of a specific type. Higher numbers mean higher chances of that type
 * being dropped. Items with the same weight assigned to them will have the same chance of being
 * dropped.
 * 
 * Please use positive integers when specifying weights.
 */
export interface ItemWeightsConfig {
  /**
   * Specifies the weight that a bomb item will drop. Defaults to `10`.
   */
  bomb?: number,

  /**
   * Specifies the weight that a cannon item will drop. Defaults to `6`.
   */
  cannon?: number,

  /**
   * Specifies the weight that a fast-balls item will drop. Defaults to `30`.
   */
  fast?: number,

  /**
   * Specifies the weight that a grow-paddle item will drop. Defaults to `20`.
   */
  grow?: number,

  /**
   * Specifies the weight that a grow-paddle item will drop. Defaults to `10`.
   */
  power?: number,

  /**
   * Specifies the weight that a shrink-paddle item will drop. Defaults to `15`.
   */
  shrink?: number,

  /**
   * Specifies the weight that a sticky-paddle item will drop. Defaults to `8`.
   */
  sticky?: number,

  /**
   * Specifies the weight that a time item will drop. Defaults to `3`.
   */
  time?: number,

  /**
   * Specifies the weight that a sticky-paddle item will drop. Defaults to `25`.
   */
  triple?: number,
};
 
/**
 * Specifies data about who often items are dropped from breaking bricks, and the probabilities
 * of spawning specific types of items.
 */
export interface ItemDropsConfig {
  /**
   * Chance that an item is dropped from a brick when it's destroyed. Values from 0.0 (0 percent)
   * to 1.0 (100%)
   * 
   * Defaults to `0.15`.
   */
  chance?: number;

  /**
   * @see {@link ItemWeightsConfig}
   */
  weights?: ItemWeightsConfig;
}

/**
 * Specifies game config properties for "color-pin" game.
 */
export default interface IShardsGameConfig {
  /**
   * Duration, in ms, that the paddle cannons effect is active.
   * 
   * Defaults to `6000`.
   */
  cannonsDurationMs?: number;

  /**
   * Rate, in ms, at which the cannons will fire continuously.
   * 
   * Defaults to `250`.
   */
  cannonsFireRateMs?: number;

  /**
   * Should the bricks quad tree be visible?
   * 
   * Defaults to `false`.
   */
  debugShowBricksQuadTree?: boolean;

  /**
   * Should the physics bodies be visible?
   * 
   * Defaults to `false`.
   */
  debugShowPhysicsBodies?: boolean;

  /**
   * Duration, in ms, that the fast-balls effect is active.
   * 
   * Defaults to `8000`.
   */
  fastBallsDurationMs?: number;

  /**
   * Speed of normal ball.
   * 
   * Defaults to `800`.
   */
  fastBallSpeed?: number;

  /**
   * Specifies data about who often items are dropped from breaking bricks, and the probabilities
   * of spawning specific types of items.
   * 
   * @see {@link ItemDropsConfig}
   */
  itemDrops?: ItemDropsConfig;

  /**
   * Scale (display size) of the item.
   * 
   * Defaults to `1.5`, meaning one and a half times their original size.
   */
  itemScale?: number;

  /**
   * Speed of dropping items.
   * 
   * Defaults to `500`.
   */
  itemSpeed?: number;

  /**
   * Max number of balls that can be active.
   * 
   * Defaults to `6`.
   */
  maxBalls?: number;

  /**
   * Multipler per active balls when calculating points awarded for each brick destroyed.
   * 
   * Increments for each active ball.
   * 
   * Defaults to `1.0`.
   */
  multiBallPointsMultiplier?: number;

  /**
   * Speed of normal ball.
   * 
   * Defaults to `400`.
   */
  normalBallSpeed?: number;

  /**
   * Number of points lost each time a ball is missed.
   * 
   * If this value is a number, that number is subtracted from the score.
   * 
   * If this value is a string, that number is a percentage of the score deducted. For example, if
   * the value is `'0.5'`, each time a ball is lost, the player loses 50, or half (!!) of their
   * current points.
   * 
   * Note: Do not set negative numbers or negative percentage strings.
   * 
   * Defaults to `0.25`, meaning the player loses a quarter of their points.
   */
  pointsLost?: number | string;

  /**
   * Base points awarded for each brick destroyed.
   * 
   * The final score is:
   * 
   * [pointsPerBrick] * [multiBallPointsMultiplier]
   * 
   * Defaults to `1`.
   */
  pointsPerBrick?: number;

  /**
   * Duration, in ms, that the power-balls effect is active.
   * 
   * Defaults to `9000`.
   */
  powerBallDurationMs?: number;

  /**
   * Duration, in ms, that the grow and shrink effect are active.
   * 
   * Default is `10000`.
   */
  sizeDurationMs?: number;

   /**
   * The game start time, in seconds.
   * 
   * Defaults to `90`.
   */
  startTimeInSeconds?: number;

  /**
   * Duration, in ms, that the sticky effect is active.
   * 
   * Default is `12000`.
   */
  stickyDurationMs?: number;

   /**
   * Time, in seconds, added to the the game timer when the time item is collected.
   * 
   * Defaults to `30`.
   */
  timeBonusSeconds?: number;
}

// ================================================================================================
function resolveWeights(dataOut?: ItemWeightsConfig): ItemWeightsConfig {
  if (!dataOut) {
    dataOut = {};
  }

  dataOut.bomb = getIntProperty(dataOut, 'bomb', 10);
  dataOut.cannon = getIntProperty(dataOut, 'cannon', 6);
  dataOut.fast = getIntProperty(dataOut, 'fast', 30);
  dataOut.grow = getIntProperty(dataOut, 'grow', 20);
  dataOut.power = getIntProperty(dataOut, 'power', 10);
  dataOut.shrink = getIntProperty(dataOut, 'shrink', 15);
  dataOut.sticky = getIntProperty(dataOut, 'sticky', 8);
  dataOut.time = getIntProperty(dataOut, 'time', 3);
  dataOut.triple = getIntProperty(dataOut, 'triple', 25);

  return (dataOut);
}

// ================================================================================================
function resolveItemDrops(dataOut?: ItemDropsConfig): ItemDropsConfig {
  if (!dataOut) {
    dataOut = {};
  }

  dataOut.chance = getNumberProperty(dataOut, 'chance', 0.15);
  dataOut.weights = getProperty(dataOut, 'weights', resolveWeights(dataOut.weights));

  return (dataOut);
}

// ================================================================================================
export function resolveConfig(dataOut?: IShardsGameConfig): IShardsGameConfig {
  if (!dataOut) {
    dataOut = {};
  }

  dataOut.cannonsDurationMs = getIntProperty(dataOut, 'cannonsDurationMs', 6 * 1000);
  dataOut.cannonsFireRateMs = getIntProperty(dataOut, 'cannonsFireRateMs', 250);
  dataOut.debugShowBricksQuadTree = getBooleanProperty(dataOut, 'debugShowBricksQuadTree', false);
  dataOut.debugShowPhysicsBodies = getBooleanProperty(dataOut, 'debugShowPhysicsBodies', false);
  dataOut.fastBallsDurationMs = getIntProperty(dataOut, 'fastBallsDurationMs', 8 * 1000);
  dataOut.fastBallSpeed = getIntProperty(dataOut, 'fastBallSpeed', 800);
  dataOut.itemDrops = getProperty(dataOut, 'itemDrops', resolveItemDrops(dataOut.itemDrops));
  dataOut.itemScale = getNumberProperty(dataOut, 'itemScale', 1.5);
  dataOut.itemSpeed = getIntProperty(dataOut, 'itemSpeed', 500);
  dataOut.maxBalls = getIntProperty(dataOut, 'maxBalls', 6);
  dataOut.multiBallPointsMultiplier = getNumberProperty(dataOut, 'multiBallPointsMultiplier', 1.0);
  dataOut.normalBallSpeed = getIntProperty(dataOut, 'normalBallSpeed', 400);
  dataOut.pointsLost = getProperty(dataOut, 'pointsLost', '0.25');
  dataOut.pointsPerBrick = getIntProperty(dataOut, 'pointsPerBrick', 1);
  dataOut.powerBallDurationMs = getIntProperty(dataOut, 'powerBallDurationMs', 9 * 1000);
  dataOut.sizeDurationMs = getIntProperty(dataOut, 'sizeDurationSeconds', 10 * 1000);
  dataOut.startTimeInSeconds = getIntProperty(dataOut, 'startTimeInSeconds', 90);
  dataOut.stickyDurationMs = getIntProperty(dataOut, 'stickyDurationMs', 12 * 1000);
  dataOut.timeBonusSeconds = getIntProperty(dataOut, 'timeBonusSeconds', 30);

  return (dataOut);
}
