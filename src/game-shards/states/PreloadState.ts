export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-shards/';

  private static readonly PHYSICS_PATH = `${PreloadState.BASE_PATH}/physics/`;

  private static readonly SOUNDS_PATH = `${PreloadState.BASE_PATH}/sounds/`;

  private game: Phaser.Game;

  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar',
    );

    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadAtlases();
    this.loadSpritesheets();
    this.loadImages();
    this.loadSounds();
    this.loadFonts();
    this.loadJsons();
    this.loadPhysics();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadAtlas(key: string): void {
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.atlas(key, `${path}${key}.png`, `${path}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlases(): void {
    [
      'shards',
    ].forEach((key) => {
      this.loadAtlas(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadFont(key: string): void {
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFontBfg(key: string): void {
    // Loads fonts created by Bitmap Font Generator. BFG appends a "_x"-like template onto the 
    // ends of its image files, which doesn't exactly match key (which doesn't use the template).
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}_0.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFonts(): void {
    [
    ].forEach((key) => {
      this.loadFont(key);
    });

    [].forEach((key) => {
      this.loadFontBfg(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadImage(key: string): void {
    this.game.load.image(key, `${PreloadState.BASE_PATH}${key}.png`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadImages(): void {
    [
      'background_01',
      'background_04',
      'background_05',
      'background_10',
      'background_11',
      'background_17',
      'node_square',
    ].forEach((key) => {
      this.loadImage(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    this.game.load.json(key, `${PreloadState.BASE_PATH}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsons(): void {
    [
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadPhysics(): void {
    [
      'paddle',
    ].forEach((key) => {
      this.loadPhysicsUnit(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadPhysicsUnit(key: string): void {
    this.game.load.physics(key, `${PreloadState.PHYSICS_PATH}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.SOUNDS_PATH}${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl,
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    [
      'snd_ball_lost',
      "snd_ball_sticky",
      "snd_ball_sticky_hit",
      "snd_bomb_explosion",
      "snd_border_hit_1",
      "snd_border_hit_2",
      "snd_border_hit_3",
      "snd_brick_damaged_1",
      "snd_brick_damaged_2",
      "snd_brick_damaged_3",
      "snd_brick_destroyed_1",
      "snd_cannon_explosion",
      "snd_cannon_fire",
      "snd_cannon_off",
      "snd_cannon_on",
      "snd_click",
      "snd_enemy_border_hit_1",
      "snd_enemy_border_hit_2",
      "snd_enemy_border_hit_3",
      "snd_enemy_brick_damaged_1",
      "snd_enemy_brick_damaged_2",
      "snd_enemy_brick_damaged_3",
      "snd_enemy_explosion",
      "snd_enemy_unbreakable_hit_1",
      "snd_enemy_unbreakable_hit_2",
      "snd_fire",
      "snd_freeze",
      "snd_game_over",
      "snd_impact",
      "snd_life",
      'snd_paddle_deff',
      "snd_paddle_enlarge",
      "snd_paddle_hit_1",
      "snd_paddle_hit_2",
      "snd_paddle_hit_3",
      "snd_paddle_shrink",
      "snd_powerball",
      "snd_split_ball",
      "snd_swoosh",
      "snd_tick",
      "snd_unbreakable_hit_1",
      "snd_unbreakable_hit_2",
    ].forEach((key) => {
      this.loadSound(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheet(key: string, frameWidth: number, frameHeight: number): void {
    const url = `${PreloadState.BASE_PATH}${key}.png`;
    this.game.load.spritesheet(key, url, frameWidth, frameHeight);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheets(): void {
    [
    ].forEach((data) => {
      this.loadSpritesheet(<string> data[0], <number> data[1], <number> data[2]);
    });
  }
}