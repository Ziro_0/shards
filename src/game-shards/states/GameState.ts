import AudioPlayer from '../classes/audio/AudioPlayer';
import LostLivesHeart from '../classes/ui/LostLivesHeart';
import PlayerInputs from '../classes/ui/PlayerInputs';
import { Game } from '../game';
import Listeners from '../Listeners';
import Ball from '../classes/entities/Ball';
import PhysicsSystem from '../classes/systems/PhysicsSystem';
import EffectsSystem from '../classes/systems/EffectsSystem';
import { EffectIds } from '../classes/entities/data/effectUtils';
import { listenerCallback, removeFromArray } from '../util/util';
import IShardsGameConfig from '../IShardsGameConfig';
import Paddle from '../classes/entities/Paddle';
import LevelsSystem, { ILevelCreatedData } from '../classes/systems/LevelsSystem';
import DesignData from '../classes/entities/data/DesignData';
import Brick from '../classes/entities/Brick';
import Item from '../classes/entities/Item';
import Missile from '../classes/entities/Missile';
import BricksQuadTreeSystem from '../classes/systems/BricksQuadTreeSystem';
import ItemSpawnerSystem from '../classes/systems/ItemSpawnerSystem';
import ScoringSystem from '../classes/systems/ScoringSystem';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _audioPlayer: AudioPlayer;

  private _playerInputs: PlayerInputs;

  private _backgroundGroup: Phaser.Group;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _physicsSystem: PhysicsSystem;

  private _effectsSystem: EffectsSystem;

  private _levelsSystem: LevelsSystem;

  private _itemSpawnerSystem: ItemSpawnerSystem;

  private _bricksQuadTreeSystem: BricksQuadTreeSystem;

  private _scoringSystem: ScoringSystem;

  private _paddle: Paddle;

  private _createdLevelData: ILevelCreatedData;

  private _timer: Phaser.Timer;

  private _playerLevel: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(_game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();

      this.initAudioPlayer();

      this.initGroups();

      this.initSystems();

      this.initUi();

      this.startGame();

      listenerCallback(this.gameObject, Listeners.READY, this.game);
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game> this.game);
  }

  // ----------------------------------------------------------------------------------------------
  render(_game: Phaser.Game): void {
    if (!this.config.debugShowBricksQuadTree) {
      return;
    }

    if (this._bricksQuadTreeSystem) {
      this._bricksQuadTreeSystem.render();
    }
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this.audio.dispose();
    this._playerInputs.destroy();
    this._effectsSystem.destroy();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private collectItem(item: Item): void {
    const effectId = item.effectId;
    item.unpresent();

    this._effectsSystem.activate(effectId);
  }

  // ----------------------------------------------------------------------------------------------
  private get config(): IShardsGameConfig {
    return (this.gameObject.shardsConfig);
  }

  // ----------------------------------------------------------------------------------------------
  private createBall(x?: number, y?: number): Ball {
    if (x === undefined) {
      x = this.game.world.centerX;
    }

    if (y === undefined) {
      y = this.game.world.centerY;
    }

    const ball = new Ball(this.gameObject, x, y, this._entitiesGroup, this._audioPlayer,
      this._bricksQuadTreeSystem);

    const SHOULD_SET_COLLISION_DATA = true;
    this.setupBall(ball, SHOULD_SET_COLLISION_DATA);

    return (ball);
  }

  // ----------------------------------------------------------------------------------------------
  private createMissile(isLeftMissile: boolean): Missile {
    const power = 1;

    const missile = new Missile(this.game, this._paddle, isLeftMissile, power,
      this._entitiesGroup);

    missile.onExploded.addOnce(this.onMissileExploded, this);

    this._physicsSystem.setMissileCollisionData(missile);
      return (missile);
  }

  // ----------------------------------------------------------------------------------------------
  private createPaddle(): void {
    this._paddle = new Paddle(this.game, this._entitiesGroup, this._physicsSystem, this.audio);
    this._physicsSystem.setPaddleCollisionData(this._paddle);

    this._paddle.onComplete.addOnce(this.onPaddleComplete, this);
    this._paddle.onFired.add(this.onPaddleFired, this);

    this._paddle.physicsBody.onBeginContact.add(this.onPaddleBeginContact, this);

    this._paddle.setWallMovementBounds(this._createdLevelData.borders);

    this._effectsSystem.paddle = this._paddle;
  }

  // ----------------------------------------------------------------------------------------------
  private destroyAllItems(): void {
    const items: Item[] = [];

    this._entitiesGroup.forEach((gameObject: PIXI.DisplayObject) => {
      if (gameObject instanceof Item) {
        items.push(gameObject);
      }
    });

    items.forEach((item) => {
      item.destroy();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private fireBall(): void {
    const balls = this._paddle.fire();
    if (balls.length > 0) {
      this.audio.play('snd_fire');

      balls.forEach((ball) => {
        const SHOULD_SET_COLLISION_DATA = false;
        this.setupBall(ball, SHOULD_SET_COLLISION_DATA);
      });
    }
  }

  // ----------------------------------------------------------------------------------------------
  private getAllBalls(): Ball[] {
    const balls: Ball[] = [];

    this._entitiesGroup.forEach((gameObject: PIXI.DisplayObject) => {
      if (gameObject instanceof Ball) {
        balls.push(gameObject);
      }
    });

    return (balls);
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    const group = this.game.add.group();
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._entitiesGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayerInputs(): void {
    this._playerInputs = new PlayerInputs(this.game, this._uiGroup, this.calcViewOffset());
    this._playerInputs.fireSignal.add(() => {
      this.fireBall();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initSystems(): void {
    const viewOffset = this.calcViewOffset();
    const game = this.gameObject;

    this._physicsSystem = new PhysicsSystem(game);

    this._effectsSystem = new EffectsSystem(game, viewOffset, this._entitiesGroup,
      this._uiGroup, this._audioPlayer, this._physicsSystem);

    this._effectsSystem.onBallsAdded.add(this.onEffectsBallsAdded, this);

    this._levelsSystem = new LevelsSystem(game, viewOffset, this._entitiesGroup, this._uiGroup,
      this._backgroundGroup, this._physicsSystem, this._audioPlayer);

    this._itemSpawnerSystem = new ItemSpawnerSystem(game, this._entitiesGroup,
      this._physicsSystem);

    this._bricksQuadTreeSystem = new BricksQuadTreeSystem(game);

    this._scoringSystem = new ScoringSystem(game);
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initPlayerInputs();
  }

  // ----------------------------------------------------------------------------------------------
  private loadNewBall(): void {
    this._paddle.unpresentCannons();

    const ball = this.createBall();
    this._paddle.loadBall(ball);
  }

  // ----------------------------------------------------------------------------------------------
  private lostAllBalls(): void {
    this.destroyAllItems();
    this._effectsSystem.deactivateAll();
    this.loadNewBall();
    this._scoringSystem.numActiveBalls = 1;
  }

  // ----------------------------------------------------------------------------------------------
  private nextLevel(): void {
    this.setLevel(0);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallOutOfBounds(ball: Ball): void {
    this._scoringSystem.lostBall();

    this.audio.play('snd_ball_lost');

    ball.destroy();

    const numActiveBalls = this.getAllBalls().length;

    if (numActiveBalls === 0) {
      this.lostAllBalls();
    } else {
      this._scoringSystem.numActiveBalls = numActiveBalls;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onBrickDeff(brick: Brick): void {
    this._scoringSystem.brickDeff();

    this._itemSpawnerSystem.spawn(brick.x, brick.y);

    removeFromArray(this._createdLevelData.bricks, brick);
    this._bricksQuadTreeSystem.removeBricks([brick]);

    if (this._createdLevelData.bricks.length === 0) {
      this.nextLevel();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onEffectsBallsAdded(balls: Ball[]): void {
    balls.forEach((ball) => {
      const SHOULD_SET_COLLISION_DATA = true;
      this.setupBall(ball, SHOULD_SET_COLLISION_DATA);
    });

    this._scoringSystem.numActiveBalls = this.getAllBalls().length;
  }

  // ----------------------------------------------------------------------------------------------
  private onMissileExploded(): void {
    this.audio.play('snd_cannon_explosion');
  }

  // ----------------------------------------------------------------------------------------------
  private onPaddleBeginContact(otherBody: Phaser.Physics.P2.Body): void {
    const sprite = otherBody ? otherBody.sprite : null;

    if (sprite instanceof Item) {
      this.collectItem(sprite);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onPaddleComplete(): void {
    this._scoringSystem.lostBall();

    this.createPaddle();
    this.loadNewBall();
  }

  // ----------------------------------------------------------------------------------------------
  private onPaddleFired(): void {
    this.audio.play('snd_cannon_fire');
    this.createMissile(true);
    this.createMissile(false);
  }

  // ----------------------------------------------------------------------------------------------
  // TODO
  private presentLostLivesHeart(value = -1): void {
    const heart = new LostLivesHeart(this.game, 400, 80 + this.calcViewOffset(), value);
    this._uiGroup.add(heart);
  }

  // ----------------------------------------------------------------------------------------------
  private setLevel(level: number): void {
    const levelClass = DesignData.LEVELS[level];
    if (!levelClass) {
      return;
    }

    let createdLevelData: ILevelCreatedData;

    try {
      createdLevelData = this._levelsSystem.build(levelClass);
    } catch (error) {
      console.error(error);
      return;
    }

    this._createdLevelData = createdLevelData;

    this.setLevelBricks();

    this._playerLevel = level;
  }

  // ----------------------------------------------------------------------------------------------
  private setLevelBricks(): void {
    const bricks = this._createdLevelData.bricks;
    bricks.forEach((brick) => {
      brick.onDeff.addOnce(this.onBrickDeff, this);
    });

    this._bricksQuadTreeSystem.addBricks(bricks);
  }

  // ----------------------------------------------------------------------------------------------
  private setupBall(ball: Ball, shouldSetCollisionData: boolean): void {
    if (!ball) {
      return;
    }

    if (shouldSetCollisionData) {
      this._physicsSystem.setBallCollisionData(ball);
    }

    if (!ball.events.onOutOfBounds.has(this.onBallOutOfBounds, this)) {
      ball.events.onOutOfBounds.addOnce(this.onBallOutOfBounds, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, 'incorrect-orientation-message', undefined,
      this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;

    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    this.setLevel(0);

    this.createPaddle();
    this.loadNewBall();

    listenerCallback(this.gameObject, Listeners.SET_SECONDS,
      this.gameObject.shardsConfig.startTimeInSeconds);
  }
}
