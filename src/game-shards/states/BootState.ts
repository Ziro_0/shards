export default class BootState {
    game: Phaser.Game;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    init() {
        // low-quality responsive scaling
        this.game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;

        // Center the game
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;

        // Portrait orientation
        this.game.scale.forceOrientation(false, true);
    }

    preload() {
        this.game.load.image('preload_bar', 'assets/game-laddu/preload_bar.png');
    }

    create() {
        this.game.state.start('PreloadState');
    }
}
