enum Listeners {
  ADD_POINT = 'add-point',
  GET_POINTS = 'get-points',
  LIVES_LOST = 'lives-lost',
  READY = 'ready',
  SET_HEARTS = 'set-hearts',
  SET_SECONDS = 'set-seconds',
}

export default Listeners;
