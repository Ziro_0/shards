export default class PlayerInputs extends Phaser.Signal {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static DEBUG_SHOW_ZONES = false;

  fireSignal: Phaser.Signal;

  game: Phaser.Game

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group, viewOffset = 0) {
    super();
    this.game = game;
    this.fireSignal = new Phaser.Signal();
    this.initZone(0, viewOffset, group, this.fireSignal, 0xff0000);
    this.inputKeys();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.fireSignal.dispose();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initKey(keyCode: number, signal: Phaser.Signal): void {
    const key = this.game.input.keyboard.addKey(keyCode);
    key.onDown.add(() => {
      signal.dispatch(this);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private inputKeys(): void {
    this.initKey(Phaser.KeyCode.SPACEBAR, this.fireSignal);
    this.initKey(Phaser.KeyCode.ENTER, this.fireSignal);
  }

  // ----------------------------------------------------------------------------------------------
  private initZone(x: number, viewOffset: number, group: Phaser.Group, signal: Phaser.Signal,
    debugColor?: number): void {
    const zone = this.game.add.graphics(x, 0 + viewOffset, group);
    if (PlayerInputs.DEBUG_SHOW_ZONES && debugColor !== undefined) {
      zone.beginFill(debugColor, 0.3);
    } else {
      zone.beginFill(0, 0);
    }

    zone.drawRect(0, 0, this.game.scale.width, this.game.scale.height);
    zone.endFill();

    zone.inputEnabled = true;
    zone.events.onInputDown.add(() => {
      signal.dispatch(this);
    })
  }
}
