import { getNumberProperty } from '../../util/util';

export interface IUiFlashCreateParams {
  alpha?: number;
  color?: number;
  numFlashes?: number;
  flashOffDuration?: number;
  flashOnDuration?: number;
}

// ================================================================================================
export default class UiFlash extends Phaser.Graphics {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _timer: Phaser.Timer;

  private _numFlashes: number;

  private _flashOnDuration: number;

  private _flashOffDuration: number;

  private _started: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, params?: IUiFlashCreateParams) {
    super(game);

    params = UiFlash.CreateParams(params);

    this._numFlashes = params.numFlashes;
    this._flashOnDuration = params.flashOnDuration
    this._flashOffDuration = params.flashOffDuration

    this.beginFill(params.color, params.alpha);
    this.drawRect(0, 0, game.width, game.height);
    this.endFill();

    this._timer = game.time.create();
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, params?: IUiFlashCreateParams, group?: Phaser.Group): UiFlash {
    const uiFlash = new UiFlash(game, params);
    if (group) {
      group.add(uiFlash);
    }
    return (uiFlash);
  }

  // ----------------------------------------------------------------------------------------------
  static CreateParams(paramsOut?: IUiFlashCreateParams): IUiFlashCreateParams {
    if (!paramsOut) {
      paramsOut = {};
    }

    paramsOut.alpha = getNumberProperty(paramsOut, 'alpha', 0.4);
    paramsOut.color = getNumberProperty(paramsOut, 'color', 0xffffff);
    paramsOut.numFlashes = getNumberProperty(paramsOut, 'numFlashes', 2);
    paramsOut.flashOffDuration = getNumberProperty(paramsOut, 'flashOnDuration', 50);
    paramsOut.flashOnDuration = getNumberProperty(paramsOut, 'flashOnDuration', 25);

    return (paramsOut);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this._timer.destroy();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._started) {
      this._started = true;
      this.on();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private off(): void {
    this.visible = false;

    this._numFlashes -= 1;

    if (this._numFlashes <= 0) {
      this.destroy();
      return;
    }

    this._timer.add(this._flashOffDuration,
      () => {
        this.on();
      }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private on(): void {
    this.visible = true;
    this._timer.add(this._flashOnDuration,
      () => {
        this.off();
      }, this);
  }
}
