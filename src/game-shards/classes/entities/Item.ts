import Entity from './Entity';
import DesignData from './data/DesignData';
import { EffectIds, Effects, getEffectAnimationFrames, getEffects } from './data/effectUtils';
import { irandomRange } from '../../util/util';
import OneTimeAnimationEffect from './effects/OneTimeAnimationEffect';

export default class Item extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly SPIN_OUT_SCALE = 2.0;

  private _effectId: EffectIds;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, effectId: EffectIds,
    entitiesGroup: Phaser.Group) {
    const frames = getEffectAnimationFrames(effectId);

    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, frames[0], entitiesGroup, false);

    this._effectId = effectId;

    this.animations.add('default', frames, DesignData.ANIMATIONS_FPS, true);
    this.animations.play('default');

    this.checkWorldBounds = true;

    this.events.onOutOfBounds.addOnce(this.onOutOfBounds, this);
  }

  // ----------------------------------------------------------------------------------------------
  get effectId(): EffectIds {
    return (this._effectId);
  }

  // ----------------------------------------------------------------------------------------------
  unpresent(): void {
    this.physicsBody.destroy();

    if (this.containsBombEffect()) {
      this.explode();
    } else {
      this.spinOut();
    }
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.scale.set(this.shardsConfig.itemScale);

    const body = this.physicsBody;
    body.setRectangleFromSprite(this);

    body.fixedRotation = true;
    body.collideWorldBounds = true;
    body.dynamic = true;
    body.setZeroDamping();

    const radians = Math.PI / 2;
    const speed = this.shardsConfig.itemSpeed;
    body.velocity.x = Math.cos(radians) * speed;
    body.velocity.y = Math.sin(radians) * speed;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private containsBombEffect(): boolean {
    return (getEffects(this.effectId).indexOf(Effects.BOMB) > -1);
  }

  // ----------------------------------------------------------------------------------------------
  private explode(): void {
    const SCALE = 1.5;
    OneTimeAnimationEffect.Create(this.game, this.x, this.y, DesignData.ANIMATION_FRAMES.EXPLOSION,
      this.group, SCALE);
    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private onOutOfBounds(): void {
    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private spinOut(): void {
    const SPIN_OUT_DURATION_MS = 500;

    this.game.tweens.create(this.scale)
      .to(
        {
          x: this.scale.x * Item.SPIN_OUT_SCALE,
          y: this.scale.y * Item.SPIN_OUT_SCALE,
        },
        SPIN_OUT_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );

    const MIN_ANGLE = 75;
    const MAX_ANGLE = 115;
    const radians = Phaser.Math.degToRad(irandomRange(MIN_ANGLE, MAX_ANGLE));

    const DISTANCE = 300;

    this.game.tweens.create(this)
      .to(
        {
          x: this.x + Math.cos(radians) * DISTANCE,
          y: this.y - Math.sin(radians) * DISTANCE,
          rotation: Math.random() >= 0.5 ? Math.PI : -Math.PI,
          alpha: 0,
        },
        SPIN_OUT_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      )

      .onComplete.addOnce(() => {
        this.destroy();
      });
  }
}
