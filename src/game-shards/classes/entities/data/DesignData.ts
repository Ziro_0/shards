import Level00 from '../../levels/Level00';

export default class DesignData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly SHARDS_TEXTURE_KEY = 'shards';

  static readonly ANIMATIONS_FPS = 8;

  static readonly LEVELS = [
    Level00,
  ];

  static readonly ANIMATION_FRAMES = {
    CANNON: Phaser.Animation.generateFrameNames('GUN_', 0, 21),
    CANNON_MUZZLE: Phaser.Animation.generateFrameNames('MUZZLE_', 0, 5),
    EXPLOSION: Phaser.Animation.generateFrameNames('EXPLOSION_', 0, 8),
    MISSILE_EXPLOSION: Phaser.Animation.generateFrameNames('GUN_EXPLO_', 0, 13),
  }
}
