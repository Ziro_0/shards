export enum EffectIds {
  BOMB = 'bomb',
  CANNON = 'cannon',
  FAST = 'fast',
  GROW = 'grow',
  POWER = 'power',
  SHRINK = 'shrink',
  STICKY = 'sticky',
  TIME = 'time',
  TRIPLE= 'triple',
}

export enum Effects {
  BOMB = 'bomb',
  CANNON = 'cannon',
  FAST = 'fast',
  GROW = 'grow',
  POWER = 'power',
  SHRINK = 'shrink',
  STICKY = 'sticky',
  TIME = 'time',
  TRIPLE = 'triple',
}

// ================================================================================================
export function getEffectIds(): EffectIds[] {
  return([
    EffectIds.BOMB,
    EffectIds.CANNON,
    EffectIds.FAST,
    EffectIds.GROW,
    EffectIds.POWER,
    EffectIds.SHRINK,
    EffectIds.STICKY,
    EffectIds.TIME,
    EffectIds.TRIPLE,
  ]);
}

// ================================================================================================
export function getEffects(id: EffectIds): Effects[] {
  switch (id) {
    case EffectIds.BOMB:
      return [Effects.BOMB];

    case EffectIds.CANNON:
      return [Effects.CANNON];

    case EffectIds.FAST:
      return [Effects.FAST];

    case EffectIds.GROW:
      return [Effects.GROW];

    case EffectIds.POWER:
      return [Effects.POWER];

    case EffectIds.SHRINK:
      return [Effects.SHRINK];

    case EffectIds.STICKY:
      return [Effects.STICKY];

    case EffectIds.TIME:
      return [Effects.TIME];

    case EffectIds.TRIPLE:
      return [Effects.TRIPLE];
  }
}

// ================================================================================================
export function getEffectAnimationFrames(id: EffectIds): string[] {
  switch (id) {
    case EffectIds.BOMB:
      return ['BONUS_BOMB', 'BONUS_BOMB_1'];

    case EffectIds.CANNON:
      return ['BONUS_CANNON', 'BONUS_CANNON_1'];

    case EffectIds.FAST:
      return ['BONUS_FAST', 'BONUS_FAST_1'];

    case EffectIds.GROW:
      return ['BONUS_ENLARGE', 'BONUS_ENLARGE_1'];

    case EffectIds.POWER:
      return ['BONUS_POWER', 'BONUS_POWER_1'];

    case EffectIds.SHRINK:
      return ['BONUS_SHRINK', 'BONUS_SHRINK_1'];

    case EffectIds.STICKY:
      return ['BONUS_STICKY', 'BONUS_STICKY_1'];

    case EffectIds.TIME:
      return ['BONUS_TIME', 'BONUS_TIME_1'];

    case EffectIds.TRIPLE:
      return ['BONUS_TRIPLE', 'BONUS_TRIPLE_1'];
  }
}
