export default interface IBrickData {
  health?: number;
  isIndestructible?: boolean;
}
