import { getBooleanProperty, getNumberProperty } from '../../util/util';
import AudioPlayer from '../audio/AudioPlayer';
import DesignData from './data/DesignData';
import IBrickData from './data/IBrickData';
import Entity from './Entity';
import IBrickDamager from './IBrickDamager';

export default class Brick extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly HIT_FLASH_DURATION_MS = 75;

  private _onDeff: Phaser.Signal;

  private _brickImageFrames: string[];

  private _brickData: IBrickData;

  private _audio: AudioPlayer;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, key: string, frame: string,
    group: Phaser.Group, brickData: IBrickData, audio: AudioPlayer) {
    super(game, x, y, key, frame, group, true);

    this._onDeff = new Phaser.Signal();

    this.brickData = brickData;
    this._audio = audio;

    this.initBrickImageFrames();
  }

  // ----------------------------------------------------------------------------------------------
  get brickData(): IBrickData {
    return (this._brickData);
  }

  // ----------------------------------------------------------------------------------------------
  set brickData(value: IBrickData) {
    this._brickData = Brick.ResolveBrickData(value);
  }

  // ----------------------------------------------------------------------------------------------
  deff(): void {
    this.onDeff.dispatch(this);

    this.playSound('snd_brick_destroyed_1');

    this.presentLargeShardParticles();
    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this._onDeff.dispose();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  hit(damager: IBrickDamager): void {
    if (!damager) {
      return;
    }

    this.brickData.health -= damager.power;
    if (this.brickData.health <= 0) {
      this.deff();
    } else {
      this.damaged();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get onDeff(): Phaser.Signal {
    return (this._onDeff);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private damaged(): void {
    this.playSound([
      'snd_brick_damaged_1',
      'snd_brick_damaged_2',
      'snd_brick_damaged_3',
    ]);

    this.hitFlash();
    this.updateDamageSprite();
    this.presentSmallShardParticles();
  }

  // ----------------------------------------------------------------------------------------------
  private hitFlash(): void {
    const image = this.game.make.image(0, 0, DesignData.SHARDS_TEXTURE_KEY, 'brick_hit');
    image.anchor.copyFrom(this.anchor);

    this.addChild(image);

    const timer = this.game.time.create()
    timer.start();
    timer.add(Brick.HIT_FLASH_DURATION_MS, () => {
      image.destroy();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initBrickImageFrames(): void {
    this._brickImageFrames = [];
    for (let index = 1; index <= 4; index += 1) {
      this._brickImageFrames.push(`${this.frameName}${index.toString(10)}`);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private playSound(soundKeys: string | string[]): void {
    if (this._audio) {
      this._audio.play(soundKeys);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private presentLargeShardParticles(): void {
    const emitter = this.game.add.emitter(this.x, this.y, 18);
    const shardSpriteFrames = Phaser.Animation.generateFrameNames('Shards_', 0, 59);
    emitter.makeParticles(DesignData.SHARDS_TEXTURE_KEY, shardSpriteFrames);
    emitter.setScale(-2, 2, 1, 1, 3000, Phaser.Easing.Sinusoidal.InOut, true);
    emitter.explode(2000);
  }

  // ----------------------------------------------------------------------------------------------
  private presentSmallShardParticles(): void {
    const emitter = this.game.add.emitter(this.x, this.y, 10);
    const shardSpriteFrames = Phaser.Animation.generateFrameNames('Shards_', 0, 59);
    emitter.makeParticles(DesignData.SHARDS_TEXTURE_KEY, shardSpriteFrames);
    emitter.setScale(-1, 1, 1, 1, 3000, Phaser.Easing.Sinusoidal.InOut, true);
    emitter.explode(1000);
  }

  // ----------------------------------------------------------------------------------------------
  private static ResolveBrickData(value: IBrickData): IBrickData {
    if (!value) {
      value = {};
    }

    value.health = getNumberProperty(value, 'health', 1);
    value.isIndestructible = getBooleanProperty(value, 'isIndestructible', false);

    return (value);
  }

  // ----------------------------------------------------------------------------------------------
  private updateDamageSprite(): void {
    if (this._brickImageFrames.length) {
      this.frameName = this._brickImageFrames.shift();
    }
  }
}
