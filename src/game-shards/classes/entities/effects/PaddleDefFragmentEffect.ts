import { irandomRange, randomRange } from '../../../util/util';

export default class PaddleDefFragmentEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MIN_DURATION_MS = 1000;

  private static readonly MAX_DURATION_MS = 2500;

  private static readonly MIN_DISTANCE = 25;

  private static readonly MAX_DISTANCE = 250;

  private static readonly MIN_SCALE = 0.6;

  private static readonly MAX_SCALE = 1.4;

  private _bmpData: Phaser.BitmapData;

  private _blinkTicks: number;

  private _isBlinking: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, bmpData: Phaser.BitmapData,
    projectionRadians: number, parent: PIXI.DisplayObjectContainer) {
    super(game, x, y, bmpData);

    this.anchor.set(0.5);

    this._bmpData = bmpData;
    this._blinkTicks = 0;

    this.setScale();

    parent.addChild(this);

    this.createTweens(projectionRadians);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    if (this._bmpData) {
      this._bmpData.destroy();
      this._bmpData = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    super.update();
    this.updateBlink();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private canBlink(): boolean {
    return (Math.random() >= 0.95);
  }

  // ----------------------------------------------------------------------------------------------
  private createMotionTween(duration: number, projectionRadians: number): void {
    const distance = irandomRange(
      PaddleDefFragmentEffect.MIN_DISTANCE,
      PaddleDefFragmentEffect.MAX_DISTANCE,
    );

    this.game.tweens.create(this)
      .to(
        {
          x: this.x + Math.cos(projectionRadians) * distance,
          y: this.y + Math.sin(projectionRadians) * distance,
          rotation: Math.PI * 2 * Math.random(),
        },
        duration,
        Phaser.Easing.Power1,
        true,
      )

      .onComplete.addOnce(() => {
        this.destroy()
      });
  }


  // ----------------------------------------------------------------------------------------------
  private createScalingTween(duration: number): void {
    this.game.tweens.create(this.scale)
      .to(
        {
          x: -this.scale.x,
        },
        duration,
        Phaser.Easing.Sinusoidal.InOut,
        true,
        0,
        -1,
        true,
      );
  }

  // ----------------------------------------------------------------------------------------------
  private createTweens(projectionRadians: number): void {
    const duration = irandomRange(
      PaddleDefFragmentEffect.MIN_DURATION_MS,
      PaddleDefFragmentEffect.MAX_DURATION_MS,
    );

    this.createScalingTween(duration);
    this.createMotionTween(duration, projectionRadians);
  }

  // ----------------------------------------------------------------------------------------------
  private setScale(): void {
    const scale = randomRange(
      PaddleDefFragmentEffect.MIN_SCALE,
      PaddleDefFragmentEffect.MAX_SCALE,
    );

    this.scale.set(scale);
  }

  // ----------------------------------------------------------------------------------------------
  private startBlink(): void {
    this._isBlinking = true;
    this._blinkTicks = 4;
    this.visible = false;
  }

  // ----------------------------------------------------------------------------------------------
  private updateBlink(): void {
    if (this._isBlinking) {
      this.updateBlinkContinue();
    } else if (this.canBlink()) {
      this.startBlink();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateBlinkContinue(): void {
    this._blinkTicks -= 1;
    if (this._blinkTicks <= 0) {
      this.visible = true;
      this._isBlinking = false;
    }
  }
}
