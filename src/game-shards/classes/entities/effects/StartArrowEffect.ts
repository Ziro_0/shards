import DesignData from '../data/DesignData';

export default class StartArrowEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly START_ANGLE = -30;

  private static readonly END_ANGLE = -150;

  private static readonly SWING_DURATION_MS = 500;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x = 0, y = 0) {
    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, 'START_ARROW');
    this.anchor.set(
      9 / this.width,
      13 / this.height,
    );

    this.angle = StartArrowEffect.START_ANGLE;

    this.game.tweens.create(this)
      .to(
        {
          angle: StartArrowEffect.END_ANGLE,
        },
        StartArrowEffect.SWING_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
        0,
        -1,
        true,
      );
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------

}
