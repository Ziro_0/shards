import DesignData from '../data/DesignData';
import CannonEffect from './CannonEffect';

export default class CannonMuzzleEffect extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, cannon: CannonEffect, container: PIXI.DisplayObjectContainer) {
    super(game, 0, 0, DesignData.SHARDS_TEXTURE_KEY, DesignData.ANIMATION_FRAMES.CANNON_MUZZLE[0]);

    this.anchor.set(0.5, 1);

    container.addChild(this);

    this.alignTo(cannon, Phaser.TOP_CENTER);

    this.playAnimation();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, cannon: CannonEffect,
    container: PIXI.DisplayObjectContainer): CannonMuzzleEffect {
    return (new CannonMuzzleEffect(game, cannon, container));
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private playAnimation(): void {
    const SPEED = 3;
    const animation = this.animations.add('default', DesignData.ANIMATION_FRAMES.CANNON_MUZZLE,
      DesignData.ANIMATIONS_FPS * SPEED);
    animation.play();

    this.events.onAnimationComplete.addOnce(() => {
      this.destroy();
    });
  }
}
