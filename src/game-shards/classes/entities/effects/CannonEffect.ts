import DesignData from '../data/DesignData';
import CannonMuzzleEffect from './CannonMuzzleEffect';

export default class CannonEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _animation: Phaser.Animation;

  private _timer: Phaser.Timer;

  private _onFired:Phaser.Signal;

  private _fireRateMs: number;

  // since both cannons operate in sync, the "main" cannon is the one on which certain events it
  // emits should be handled
  private _isMainCannon: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, container: PIXI.DisplayObjectContainer,
    isMainCannon: boolean, fireRateMs: number) {
    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, DesignData.ANIMATION_FRAMES.CANNON[0]);

    this._isMainCannon = isMainCannon;
    this._fireRateMs = fireRateMs;

    this._onFired = new Phaser.Signal();

    this.anchor.set(0.5, 1);

    this.createTimer();
    this.createAnimations();

    container.addChild(this);

    this.present();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this._onFired.dispose();
    this._timer.destroy();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get onFired(): Phaser.Signal {
    return (this._onFired);
  }

  // ----------------------------------------------------------------------------------------------
  unpresent(): void {
    this._timer.removeAll();

    this._animation.reverse();
    this._animation.play();

    this.events.onAnimationComplete.addOnce(() => {
      this.destroy();
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createAnimations(): void {
    this._animation = this.animations.add('default', DesignData.ANIMATION_FRAMES.CANNON,
      DesignData.ANIMATIONS_FPS);
    this._animation.speed = DesignData.ANIMATIONS_FPS;
  }

  // ----------------------------------------------------------------------------------------------
  private createTimer(): void {
    this._timer = this.game.time.create();
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private present(): void {
    this._animation.play();
    this.events.onAnimationComplete.addOnce(() => {
      this.startFiring();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private startFiring(): void {
    this._timer.loop(this._fireRateMs, this.onFiringRateTimer, this);
  }

  // ----------------------------------------------------------------------------------------------
  private onFiringRateTimer(): void {
    CannonMuzzleEffect.Create(this.game, this, this.parent);
    if (this._isMainCannon) {
      this._onFired.dispatch(this);
    }
  }
}
