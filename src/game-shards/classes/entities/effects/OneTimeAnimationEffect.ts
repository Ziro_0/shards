import DesignData from '../data/DesignData';

/**
 * A sprite that plays its animation once, then destroys itself.
 * You can optionally specify the scale of the sprite and speed of the animation.
 */
export default class OneTimeAnimationEffect extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, frames: string[], group: Phaser.Group,
    scale = 1.0, speed = 1.0) {
    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, frames[0]);

    this.anchor.set(0.5);
    this.scale.set(scale);

    const animation = this.animations.add('default', frames, DesignData.ANIMATIONS_FPS);
    animation.speed = DesignData.ANIMATIONS_FPS * speed;
    animation.play();

    this.events.onAnimationComplete.addOnce(() => {
      this.destroy();
    });

    group.add(this);
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, x: number, y: number, frames: string[], group: Phaser.Group,
    scale = 1.0, speed = 1.0): OneTimeAnimationEffect {
    return (new OneTimeAnimationEffect(game, x, y, frames, group, scale, speed));
  }
}
