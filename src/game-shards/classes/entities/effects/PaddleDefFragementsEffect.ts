import { removeFromArray } from '../../../util/util';
import Paddle from '../Paddle';
import PaddleDefFragmentEffect from './PaddleDefFragmentEffect';

export default class PaddleDefFragementsEffect {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _fragments: PaddleDefFragmentEffect[];

  private _onComplete: Phaser.Signal;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(paddle: Paddle, columns: number, rows: number) {
    this._onComplete = new Phaser.Signal();
    this.createFragments(paddle, columns, rows);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this._onComplete.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  get onComplete(): Phaser.Signal {
    return (this._onComplete);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createFragments(paddle: Paddle, columns: number, rows: number): void {
    this._fragments = [];

    const bounds = paddle.getBounds();
    const cellWt = bounds.width / columns;
    const cellHt = bounds.height / rows;
    const iCellWt = Math.floor(cellWt);
    const iCellHt = Math.floor(cellHt);

    for (let row = 0; row < rows; row += 1) {
      const y = Math.floor(row * cellHt);
      for (let col = 0; col < columns; col += 1) {
        const x = Math.floor(col * cellWt);
        const bmpData = paddle.game.make.bitmapData(iCellWt, iCellHt);
        bmpData.copy(
          paddle,
          x,
          y,
          cellWt,
          cellHt,
          0,
          0,
          null, null, null,
          0,
          0,
        );

        const xFrag = x + bounds.x + cellWt / 2;
        const yFrag = y + bounds.y + cellHt / 2;
        const radians = Phaser.Math.angleBetween(paddle.x, paddle.y, xFrag, yFrag);
        const fragment = new PaddleDefFragmentEffect(
          paddle.game,
          xFrag,
          yFrag,
          bmpData,
          radians,
          paddle.parent,
        );

        this._fragments.push(fragment);
        fragment.events.onDestroy.addOnce(this.onFragmentDestroy, this);
      }
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onFragmentDestroy(fragment: PaddleDefFragmentEffect): void {
    removeFromArray(this._fragments, fragment);

    if (this._fragments.length === 0) {
      this._onComplete.dispatch(this);
    }
  }
}
