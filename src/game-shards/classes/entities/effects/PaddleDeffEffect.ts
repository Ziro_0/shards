import { irandomRange, randomRange } from '../../../util/util';
import UiFlash from '../../ui/UiFlash';
import DesignData from '../data/DesignData';
import Paddle from '../Paddle';
import OneTimeAnimationEffect from './OneTimeAnimationEffect';
import PaddleDefFragementsEffect from './PaddleDefFragementsEffect';

export default class PaddleDeffEffect extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly EXPLODING_DURATION_MS = 2000;

  private static readonly EXPLOSION_RATE_MS = 250;

  private static readonly EXPLODE_TREMOR_INTENSITY = 7;

  private static readonly EXPLODE_FRAGMENT_COLUMNS = 8;

  private static readonly EXPLODE_FRAGMENT_ROWS = 3;

  private static readonly FLASH_RATE_MS = 60;

  private _paddleFragmentsEffect: PaddleDefFragementsEffect;

  private _paddle: Paddle;

  private _timer: Phaser.Timer;

  private _onComplete: Phaser.Signal;

  private _paddleFrame: string;

  private _x: number;

  private _y: number;

  private _isFlashing: boolean;

  private _isRequestingFragments: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(paddle: Paddle) {
    super(paddle.game);

    this._onComplete = new Phaser.Signal();

    this._paddle = paddle;
    this._x = this._paddle.x;
    this._y = this._paddle.y;
    this._paddleFrame = this._paddle.frameName;

    this._timer = this._paddle.game.time.create(false);
    this._timer.start();

    this.createExplosionsTimer();
    this.createDurationTimer();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean, soft?: boolean): void {
    this._timer.destroy();
    super.destroy(destroyChildren, soft);
  }

  // ----------------------------------------------------------------------------------------------
  get onComplete(): Phaser.Signal {
    return (this._onComplete);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    super.update();

    if (this._isRequestingFragments) {
      this._isRequestingFragments = false;
      this.createPaddleFragmemts();
      this.createFinalExplosion();
    } else if (this._timer && this._timer.running) {
      this.updatePaddleEffect();
      this.updateFlashEffect();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createDurationTimer(): void {
    this._timer.add(PaddleDeffEffect.EXPLODING_DURATION_MS, () => {
      this._timer.stop();

      this._paddle.x = this._x;
      this._paddle.y = this._y;
      this._paddle.frameName = this._paddleFrame;
      this._paddle.visible = false;

      this._isRequestingFragments = true;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private createExplosionsTimer(): void {
    const bounds = this._paddle.getBounds();
    const group = this._paddle.group;

    this._timer.loop(PaddleDeffEffect.EXPLOSION_RATE_MS, () => {
      const x = bounds.randomX;
      const y = bounds.randomY;
      const scale = randomRange(0.75, 1.25);
      const speed = randomRange(0.75, 1.25);
      OneTimeAnimationEffect.Create(this._paddle.game, x, y, DesignData.ANIMATION_FRAMES.EXPLOSION,
        group, scale, speed);

      this._paddle.playSound([
        'snd_enemy_explosion',
        'snd_cannon_explosion',
        'snd_bomb_explosion',
      ]);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private createFinalExplosion(): void {
    const SCALE = 2.2;
    const SPEED = 0.7;
    OneTimeAnimationEffect.Create(this._paddle.game, this._paddle.x, this._paddle.y,
      DesignData.ANIMATION_FRAMES.EXPLOSION, this._paddle.group, SCALE, SPEED);
    this._paddle.playSound('snd_paddle_deff');

    UiFlash.Create(
      this.game,
      {
        alpha: 0.8,
        color: 0xff0000,
      },
      this._paddle.group,
    );
  }

  // ----------------------------------------------------------------------------------------------
  private createPaddleFragmemts(): void {
    this._paddleFragmentsEffect = new PaddleDefFragementsEffect(this._paddle,
      PaddleDeffEffect.EXPLODE_FRAGMENT_COLUMNS,
      PaddleDeffEffect.EXPLODE_FRAGMENT_ROWS);

    this._paddleFragmentsEffect.onComplete.addOnce(this.onPaddleFragmentsComplete, this);
  }

  // ----------------------------------------------------------------------------------------------
  private explosionFlash(): void {
    if (this._isFlashing) {
      return;
    }

    this._isFlashing = true;

    this._paddle.frameName = Math.random() >= 0.5 ? 'PADDLE_RED_FLASH' : 'PADDLE_WHITE_FLASH';

    this._timer.add(PaddleDeffEffect.FLASH_RATE_MS, () => {
      this._paddle.frameName = this._paddleFrame;
      this._isFlashing = false;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private onPaddleFragmentsComplete(): void {
    this._onComplete.dispatch(this);
  }

  // ----------------------------------------------------------------------------------------------
  private updateFlashEffect(): void {
    if (Math.random() >= 0.8) {
      this.explosionFlash();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updatePaddleEffect(): void {
    const INTEN = PaddleDeffEffect.EXPLODE_TREMOR_INTENSITY;
    this._paddle.x = this._x + irandomRange(-INTEN, INTEN);
    this._paddle.y = this._y + irandomRange(-INTEN, INTEN);
  }
}
