import Brick from './Brick';
import DesignData from './data/DesignData';
import OneTimeAnimationEffect from './effects/OneTimeAnimationEffect';
import Entity from './Entity';
import IBrickDamager from './IBrickDamager';

export default class Missile extends Entity implements IBrickDamager {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly X_OFS = 40;

  private static readonly Y_OFS = -15;

  private static readonly SPEED = 600;

  private _onExploded: Phaser.Signal;

  private _power: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, paddle: Entity, isLeftMissile: boolean, power: number,
    group: Phaser.Group) {
    const x = paddle.x + (isLeftMissile ? -Missile.X_OFS : Missile.X_OFS);
    const y = paddle.y + Missile.Y_OFS;
    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, 'LASER', group);

    this._onExploded = new Phaser.Signal();

    this.power = power;

    this.physicsBody.onBeginContact.add(this.onBeginContact, this);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this._onExploded.dispose();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get onExploded(): Phaser.Signal {
    return (this._onExploded);
  }

  // ----------------------------------------------------------------------------------------------
  get power(): number {
    return (this._power);
  }

  // ----------------------------------------------------------------------------------------------
  set power(value: number) {
    this._power = value;
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    const body = this.physicsBody;

    body.fixedRotation = true;
    body.collideWorldBounds = true;
    body.dynamic = true;
    body.setZeroDamping();

    const radians = -Math.PI / 2;
    body.velocity.x = Math.cos(radians) * Missile.SPEED;
    body.velocity.y = Math.sin(radians) * Missile.SPEED;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private explode(): void {
    OneTimeAnimationEffect.Create(this.game, this.x, this.top,
      DesignData.ANIMATION_FRAMES.EXPLOSION, this.group);

    this._onExploded.dispatch(this);

    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private onBeginContact(otherBody: Phaser.Physics.P2.Body): void {
    const sprite = otherBody ? otherBody.sprite : null;

    if (sprite instanceof Brick) {
      sprite.hit(this);
    }

    this.explode();
  }
}
