import { getChildByName, isMobile } from '../../util/util';
import AudioPlayer from '../audio/AudioPlayer';
import PhysicsSystem from '../systems/PhysicsSystem';
import Ball from './Ball';
import DesignData from './data/DesignData';
import CannonEffect from './effects/CannonEffect';
import PaddleDeffEffect from './effects/PaddleDeffEffect';
import Entity from './Entity';

export enum PADDLE_SIZE {
  NORMAL,
  LARGE,
  SMALL,
};

interface IPaddleBounds {
  left: number;
  right: number;
}

interface IAttachedBall {
  ball: Ball;
  ofsX: number;
  ofsY: number;
}

export default class Paddle extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  private static readonly CANNON_POSITIONS: [number, number][] = [
    [-40, 16],
    [40, 16],
  ];

  private static readonly Y = 1100;

  private static readonly SPEED = 50;

  private static readonly BALL_Y_OFFSET = -48;

  private _movementBounds: IPaddleBounds;

  private _cannons: CannonEffect[];

  private _attachedBalls: IAttachedBall[];

  private _onComplete: Phaser.Signal;

  private _onFired: Phaser.Signal;

  // private _ball: Ball;

  private _paddleDefEffect: PaddleDeffEffect;

  private _physicsSystem: PhysicsSystem;

  private _audio: AudioPlayer;

  private _size: PADDLE_SIZE;

  // private _ballAttachOfsX: number;

  // private _ballAttachOfsY: number;

  private _isTrackingInput: boolean;

  private _isExplodeRequested: boolean;

  private _isPowerful: boolean;

  private _isSticky: boolean;

  private _isFast: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, entitiesGroup: Phaser.Group, physicsSystem: PhysicsSystem,
    audio: AudioPlayer) {
    super(game, game.world.centerX, Paddle.Y, DesignData.SHARDS_TEXTURE_KEY, 'PADDLE',
      entitiesGroup, true);

    this._onComplete = new Phaser.Signal();
    this._onFired = new Phaser.Signal();

    this.initInput();

    this._size = PADDLE_SIZE.NORMAL;

    this._physicsSystem = physicsSystem;
    this._audio = audio;

    this._attachedBalls = [];
  }

  // ----------------------------------------------------------------------------------------------
  collidedWithBall(ball: Ball): void {
    if (this._isSticky) {
      this.adhereBall(ball);
    } else {
      this.reboundBall(ball);
    }
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this.onComplete.dispose();
    this.onFired.dispose();

    if (this._paddleDefEffect) {
      this._paddleDefEffect.destroy();
      this._paddleDefEffect = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  explode(): void {
    this._isTrackingInput = false;
    this._isExplodeRequested = true;
  }

  // ----------------------------------------------------------------------------------------------
  fire(): Ball[] {
    const firedBalls: Ball[] = [];

    while (this._attachedBalls.length) {
      const attachedBall = this._attachedBalls.pop();
      attachedBall.ball.fire();
      firedBalls.push(attachedBall.ball);
    }

    return (firedBalls);
  }

  // ----------------------------------------------------------------------------------------------
  get isFast(): boolean {
    return (this._isFast);
  }

  // ----------------------------------------------------------------------------------------------
  set isFast(value: boolean) {
    this._isFast = value;
    this.setEffectImage('PADDLE_FIRE', this._isFast);
  }

  // ----------------------------------------------------------------------------------------------
  get isPowerful(): boolean {
    return (this._isPowerful);
  }

  // ----------------------------------------------------------------------------------------------
  set isPowerful(value: boolean) {
    this._isPowerful = value;
    this.setEffectImage('PADDLE_POWER', this._isPowerful);
  }

  // ----------------------------------------------------------------------------------------------
  get isSticky(): boolean {
    return (this._isSticky);
  }

  // ----------------------------------------------------------------------------------------------
  set isSticky(value: boolean) {
    this._isSticky = value;
    this.setEffectImage('PADDLE_STICKY', this._isSticky);
  }

  // ----------------------------------------------------------------------------------------------
  loadBall(ball: Ball): void {
    if (!ball) {
      return;
    }

    const ballBody = ball.physicsBody;
    const body = this.physicsBody;

    ballBody.x = this.x;
    ballBody.y = this.y + Paddle.BALL_Y_OFFSET;

    const attachedBall: IAttachedBall = {
      ball,
      ofsX: body.x - ballBody.x,
      ofsY: body.y - ballBody.y,
    };

    this.attachBall(attachedBall);
  }

  // ----------------------------------------------------------------------------------------------
  get onComplete(): Phaser.Signal {
    return (this._onComplete);
  }

  // ----------------------------------------------------------------------------------------------
  get onFired(): Phaser.Signal {
    return (this._onFired);
  }

  // ----------------------------------------------------------------------------------------------
  playSound(soundKeys: string | string[]): void {
    if (this._audio) {
      this._audio.play(soundKeys);
    }
  }

  // ----------------------------------------------------------------------------------------------
  presentCannons(): void {
    if (this._cannons) {
      return;
    }

    this._cannons = [
      this.createCannon(0, true),
      this.createCannon(1, false),
    ]
  }

  // ----------------------------------------------------------------------------------------------
  presentSize(size: PADDLE_SIZE): boolean {
    this._attachedBalls.forEach(() => {
      this.fire();
    });

    return (this.setSize(size));
  }

  // ----------------------------------------------------------------------------------------------
  setWallMovementBounds(walls: Entity[]): void {
    let leftBounds: number;
    let rightBounds: number;

    if (walls) {
      walls.forEach((wall) => {
        if (wall.name === 'left') {
          leftBounds = wall.right;
        } else if (wall.name === 'right') {
          rightBounds = wall.left;
        }
      });
    }

    if (leftBounds === undefined || rightBounds === undefined) {
      this._movementBounds = null;
      return;
    }

    this._movementBounds = {
      left: leftBounds,
      right: rightBounds,
    };
  }

  // ----------------------------------------------------------------------------------------------
  get size(): PADDLE_SIZE {
    return (this._size);
  }

  // ----------------------------------------------------------------------------------------------
  unpresentCannons(): boolean {
    if (!this._cannons) {
      return (false);
    }

    this._cannons.forEach((cannon) => {
      cannon.unpresent();
    });

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (this._isExplodeRequested) {
      this.beginPaddleDeffEffect();
    } else {
      this._updateBall();
      this._updatePosition();
    }

    super.update();
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.physicsBody.fixedRotation = true;
    this.physicsBody.clearShapes();
    this.physicsBody.loadPolygon('paddle', 'paddle');
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private adhereBall(ball: Ball): void {
    if (!ball) {
      return;
    }

    this.playSound('snd_ball_sticky');

    const ballBody = ball.physicsBody;
    const body = this.physicsBody;

    const attachedBall: IAttachedBall = {
      ball,
      ofsX: body.x - ballBody.x,
      ofsY: body.y - ballBody.y,
    };

    this.attachBall(attachedBall);
  }

  // ----------------------------------------------------------------------------------------------
  private attachBall(attachedBall: IAttachedBall): void {
    const ball = attachedBall ? attachedBall.ball : null;
    if (!ball) {
      return;
    }

    this._attachedBalls.push(attachedBall);

    const ballBody = ball.physicsBody;
    ballBody.setZeroVelocity();
    ballBody.static = true;

    ball.presentStartArrow();

    this._updateBall();
  }

  // ----------------------------------------------------------------------------------------------
  private beginPaddleDeffEffect(): void {
    if (this._paddleDefEffect) {
      return;
    }

    this.physicsBody.destroy();

    this._paddleDefEffect = new PaddleDeffEffect(this);
    this._paddleDefEffect.onComplete.addOnce(this.onPaddleDeffEffectComplete, this);
  }

  // ----------------------------------------------------------------------------------------------
  private createCannon(index: number, isMainCannon: boolean): CannonEffect {
    const CANNON_POSITION = Paddle.CANNON_POSITIONS[index];
    const fireRateMs = this.shardsConfig.cannonsFireRateMs;
    const cannonEffect = new CannonEffect(this.game, CANNON_POSITION[0], CANNON_POSITION[1], this,
      isMainCannon, fireRateMs);

    if (isMainCannon) {
      cannonEffect.events.onDestroy.addOnce(this.onCannonDestroy, this);
      cannonEffect.onFired.add(this.onCannonFired, this);
    }

    return (cannonEffect);
  }

  // ----------------------------------------------------------------------------------------------
  private initInput(): void {
    if (isMobile(this.game)) {
      this.game.input.onDown.addOnce(() => {
        this._isTrackingInput = true;
      });
    } else {
      this._isTrackingInput = true;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onCannonDestroy(): void {
    this._cannons = null;
  }

  // ----------------------------------------------------------------------------------------------
  private onCannonFired(): void {
    this._onFired.dispatch(this);
  }

  // ----------------------------------------------------------------------------------------------
  private onPaddleDeffEffectComplete(): void {
    this._onComplete.dispatch(this);
    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private reboundBall(ball: Ball): void {
    const ballBody = ball ? ball.physicsBody : null;
    if (!ballBody) {
      return;
    }

    if (this.isSticky) {
      this.playSound('snd_ball_sticky_hit');
    } else {
      this.playSound([
        'snd_paddle_hit_1',
        'snd_paddle_hit_2',
        'snd_paddle_hit_3',
      ]);
    }

    const paddleBody = this.physicsBody;

    const HALF_PADDLE_WT = this.width / 2;

    let angle: number;

    if (ballBody.x < paddleBody.x) {
      //  Ball is on the left-hand side of the paddle
      const diff = paddleBody.x - ballBody.x;
      const ratio = diff / HALF_PADDLE_WT;
      angle = Phaser.Math.linear(91, 150, ratio);
    } else if (ballBody.x > paddleBody.x) {
      //  Ball is on the right-hand side of the paddle
      const diff = ballBody.x -paddleBody.x;
      const ratio = diff / HALF_PADDLE_WT;
      angle = Phaser.Math.linear(89, 30, ratio);
    } else {
      //  Ball is perfectly in the middle
      //  Add a little random angle to stop it from rebounding straight up
      angle = Phaser.Utils.randomChoice(89, 91);
    }

    const radians = Phaser.Math.degToRad(angle);
    ballBody.velocity.x = Math.cos(radians) * ball.speed;
    ballBody.velocity.y = -Math.abs(Math.sin(radians) * ball.speed);
  }

  // ----------------------------------------------------------------------------------------------
  private resizeShapes(): void {
    let scaleValue = 1.0;
    if (this._size === PADDLE_SIZE.LARGE) {
      scaleValue = 2.0;
    } else if (this._size === PADDLE_SIZE.SMALL) {
      scaleValue = 0.5;
    }

    this.physicsBody.clearShapes();

    const scale = new Phaser.Point(scaleValue, 1.0);
    this._physicsSystem.loadPolygon(this.physicsBody, 'paddle', 'paddle', scale);
    this._physicsSystem.setPaddleCollisionData(this);

    this.scale.set(scaleValue, this.scale.y);
  }

  // ----------------------------------------------------------------------------------------------
  private setEffectImage(frameKey: string, isActive: boolean): void {
    let image = <Phaser.Image> getChildByName(this, frameKey);
    if (isActive) {
      if (image) {
        image.visible = true;
        return;
      }

      image = this.game.make.image(0, 0, this.key, frameKey);
      image.anchor.set(0.5);
      image.name = frameKey;
      this.addChild(image);
    } else if (image) {
      image.visible = false;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setSize(value: PADDLE_SIZE): boolean {
    if (this._size === value) {
      return (false);
    }

    this._size = value;
    this.resizeShapes();
    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private _updateBall(): void {
    this._attachedBalls.forEach((attachedBall) => {
      const ball = attachedBall.ball;
      ball.physicsBody.x = this.x - attachedBall.ofsX;
      ball.physicsBody.y = this.y - attachedBall.ofsY;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private _updatePosition(): void {
    if (!this._isTrackingInput) {
      return;
    }

    const xInput = this.game.input.activePointer.worldX;
    if (!this._updatePositionLeft(xInput)) {
      this._updatePositionRight(xInput);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updatePositionLeft(xInput: number): boolean {
    const body = this.physicsBody;

    if (xInput >= body.x) {
      return (false);
    }

    let x = Math.max(body.x - Paddle.SPEED, xInput);

    if (this._movementBounds) {
      const xSprite = x + body.offset.x;
      const spriteLeft = xSprite - this.offsetX;
      const diff = spriteLeft - this._movementBounds.left;
      if (diff < 0) {
        x -= diff;
      }
    }

    body.x = x;
    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private _updatePositionRight(xInput: number): boolean {
    const body = this.physicsBody;

    if (xInput <= body.x) {
      return (false);
    }

    let x = Math.min(body.x + Paddle.SPEED, xInput);

    if (this._movementBounds) {
      const xSprite = x + body.offset.x;
      const spriteRight = xSprite + this.width - this.offsetX;
      const diff = spriteRight - this._movementBounds.right;
      if (diff > 0) {
        x -= diff;
      }
    }

    body.x = x;
    return (true);
  }
}
