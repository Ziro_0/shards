import Entity from './Entity';
import DesignData from './data/DesignData';
import StartArrowEffect from './effects/StartArrowEffect';
import Paddle from './Paddle';
import Brick from './Brick';
import IBrickDamager from './IBrickDamager';
import AudioPlayer from '../audio/AudioPlayer';
import BricksQuadTreeSystem from '../systems/BricksQuadTreeSystem';
import { Game } from '../../game';

export default class Ball extends Entity implements IBrickDamager {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly WALL_NAMES = ['right', 'left', 'top', 'topRight', 'topLeft'];

  private static readonly PHYS_BODY_RADIUS = 16;

  private static readonly EFFECTS_CHANGE_RATE_MS = 500;

  private _broadPhaseBricks: Set<Brick>;

  private _effectFrames: string[];

  private _startArrowEffect: StartArrowEffect;

  private _audio: AudioPlayer;

  private _bricksQuadTreeSystem: BricksQuadTreeSystem;

  private _effectsFrameTimerEvent: Phaser.TimerEvent;

  private _speed: number;

  private _power: number;

  private _isFast: boolean;

  private _isPowerful: boolean;

  private _isSticky: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, x: number, y: number, entitiesGroup: Phaser.Group,
    audio: AudioPlayer, bricksQuadTreeSystem: BricksQuadTreeSystem) {
    super(game, x, y, DesignData.SHARDS_TEXTURE_KEY, 'BALL_NORMAL', entitiesGroup);

    this._audio = audio;
    this._bricksQuadTreeSystem = bricksQuadTreeSystem;

    this.checkWorldBounds = true;

    this._broadPhaseBricks = new Set();

    this.physicsBody.setZeroVelocity();

    this._power = 1;

    this._effectFrames = [];

    this.isFast = false;

    this.startEffectsFrameTimer();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    if (this._effectsFrameTimerEvent) {
      this._effectsFrameTimerEvent.timer.destroy();
      this._effectsFrameTimerEvent = null;
    }

    this.game.physics.p2.setPostBroadphaseCallback(null, this);
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  fire(radians?: number): void {
    if (radians === undefined && !this._startArrowEffect) {
      return;
    }

    this.physicsBody.dynamic = true;
    this.physicsBody.setZeroDamping();

    this.physicsBody.onBeginContact.add(this.onBeginContact, this);
    this.physicsBody.onEndContact.add(this.onEndContact, this);

    const fireRadians = radians !== undefined ? radians : this._startArrowEffect.rotation;
    this._updatePhysVelocity(fireRadians);

    this.unpresentStartArrow();
  }

  // ----------------------------------------------------------------------------------------------
  get isFast(): boolean {
    return (this._isFast);
  }

  // ----------------------------------------------------------------------------------------------
  set isFast(value: boolean) {
    this._isFast = value;

    if (this._isFast) {
      this._speed = this.shardsConfig.fastBallSpeed;
    } else {
      this._speed = this.shardsConfig.normalBallSpeed;
    }

    this.addEffectsTimerFrame('BALL_FAST', this._isFast);

    this._updatePhysVelocity();
  }

  // ----------------------------------------------------------------------------------------------
  get isPowerful(): boolean {
    return (this._isPowerful);
  }

  // ----------------------------------------------------------------------------------------------
  set isPowerful(value: boolean) {
    this._isPowerful = value;
    this.addEffectsTimerFrame('BALL_POWER', this._isPowerful);
  }

  // ----------------------------------------------------------------------------------------------
  get isSticky(): boolean {
    return (this._isSticky);
  }

  // ----------------------------------------------------------------------------------------------
  set isSticky(value: boolean) {
    this._isSticky = value;
    this.addEffectsTimerFrame('BALL_STICKY', this._isSticky);
  }

  // ----------------------------------------------------------------------------------------------
  get power(): number {
    return (this._power);
  }

  // ----------------------------------------------------------------------------------------------
  set power(value: number) {
    this._power = value;
  }

  // ----------------------------------------------------------------------------------------------
  presentStartArrow(): void {
    if (!this._startArrowEffect) {
      this._startArrowEffect = new StartArrowEffect(this.game);
    }

    this.addChild(this._startArrowEffect);
  }

  // ----------------------------------------------------------------------------------------------
  get quadTreeSystem(): BricksQuadTreeSystem {
    return (this._bricksQuadTreeSystem);
  }

  // ----------------------------------------------------------------------------------------------
  get speed(): number {
    return (this._speed);
  }

  // ----------------------------------------------------------------------------------------------
  unpresentStartArrow(): void {
    if (this._startArrowEffect) {
      this._startArrowEffect.destroy();
      this._startArrowEffect = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (this._isPowerful) {
      this.updatePowerful();
    }
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.physicsBody.setCircle(Ball.PHYS_BODY_RADIUS);
    this.physicsBody.fixedRotation = true;
    this.physicsBody.collideWorldBounds = false;

    this.game.physics.p2.setPostBroadphaseCallback(this.onPhysPostBroadPhase, this);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addEffectsTimerFrame(frameKey: string, isActive: boolean): void {
    const indexOf = this._effectFrames.indexOf(frameKey);

    if (isActive) {
      if (indexOf === -1) {
        this._effectFrames.push(frameKey);
      }
    } else if (indexOf > -1) {
      this._effectFrames.splice(indexOf, 1);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private isOneBodyThisBallAndOtherPaddle(body1: Phaser.Physics.P2.Body,
  body2: Phaser.Physics.P2.Body): boolean {
    return (
      (body1.sprite === this && body2.sprite instanceof Paddle)
      ||
      (body2.sprite === this && body1.sprite instanceof Paddle)
    );
  }

  // ----------------------------------------------------------------------------------------------
  private onBeginContact(otherBody: Phaser.Physics.P2.Body): void {
    const sprite = otherBody ? otherBody.sprite : null;

    if (sprite instanceof Brick) {
      sprite.hit(this);
    } else if (Ball.WALL_NAMES.indexOf(sprite.name) > -1) {
      this._audio.play('snd_impact', null, null, 0.4);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onEndContact(otherBody: Phaser.Physics.P2.Body): void {
    const sprite = otherBody ? otherBody.sprite : null;

    if (sprite instanceof Paddle) {
      sprite.collidedWithBall(this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onPhysPostBroadPhase(body1: Phaser.Physics.P2.Body,
    body2: Phaser.Physics.P2.Body): boolean {

    if (!this.physBroadPhasePaddle(body1, body2)) {
      return (false);
    }

    return (this.physBroadPhaseBrick(body1, body2));
  }

  // ---------------------------------------------------------------------------------------------{
  private physBroadPhaseBrick(body1: Phaser.Physics.P2.Body,
    body2: Phaser.Physics.P2.Body): boolean {
    if (body1.sprite === this) {
      if (body2.sprite instanceof Brick) {
        this._broadPhaseBricks.add(body2.sprite);
      }
    } else if (body2.sprite === this) {
      if (body1.sprite instanceof Brick) {
        this._broadPhaseBricks.add(body1.sprite);
      }
    }

    return (true);
  }

  // ---------------------------------------------------------------------------------------------{
  private physBroadPhasePaddle(body1: Phaser.Physics.P2.Body,
    body2: Phaser.Physics.P2.Body): boolean {

    // broadphase only checks between this ball and paddle
    if (!this.isOneBodyThisBallAndOtherPaddle(body1, body2)) {
      return (true);
    }

    // only collide if ball is traveling downwards; if ball is going upwards, allow it to
    // pass thru paddle. this prevents issues with balling colliding with a sticky paddle
    // immendately after the ball was released from the paddle
    return (this.physicsBody.velocity.y > 0);
  }

  // ----------------------------------------------------------------------------------------------
  private static RectsIntersect(a: PIXI.Rectangle, b: PIXI.Rectangle): boolean {
    if (a.width <= 0 || a.height <= 0 || b.width <= 0 || b.height <= 0) {
      return (false);
    }

    return (!(a.right < b.x || a.bottom < b.y || a.x > b.right || a.y > b.bottom));
  };

  // ----------------------------------------------------------------------------------------------
  private startEffectsFrameTimer(): void {
    const timer = this.game.time.create();
    timer.start();

    this._effectsFrameTimerEvent = timer.loop(Ball.EFFECTS_CHANGE_RATE_MS, () => {
      if (this._effectFrames.length > 0) {
        Phaser.ArrayUtils.rotateLeft(this._effectFrames);
      }

      const frameName = this._effectFrames[0] || 'BALL_NORMAL';
      if (frameName !== this.frameName) {
        this.frameName = frameName;
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private updatePowerful(): void {
    const bounds = this.getBounds();

    const results = this._bricksQuadTreeSystem.check(this);
    const bricksToDestroy: Brick[] = [];
    results.forEach((brick, brickRect: PIXI.Rectangle) => {
      if (Ball.RectsIntersect(bounds, brickRect)) {
        bricksToDestroy.push(brick);
      }
    });

    this._bricksQuadTreeSystem.removeBricks(bricksToDestroy);

    bricksToDestroy.forEach((brick) => {
      brick.deff();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private _updatePhysVelocity(radians?: number): void {
    if (radians === undefined) {
      const point = new Phaser.Point(this.physicsBody.velocity.x, this.physicsBody.velocity.y);
      point.normalize();
      point.setMagnitude(this._speed);
      this.physicsBody.velocity.x = point.x;
      this.physicsBody.velocity.y = point.y;
    } else {
      this.physicsBody.velocity.x = Math.cos(radians) * this._speed;
      this.physicsBody.velocity.y = Math.sin(radians) * this._speed;
    }
  }
}
