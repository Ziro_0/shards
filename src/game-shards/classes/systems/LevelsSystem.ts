import { Game } from '../../game';
import AudioPlayer from '../audio/AudioPlayer';
import Brick from '../entities/Brick';
import IBrickData from '../entities/data/IBrickData';
import Entity from '../entities/Entity';
import PhysicsSystem from './PhysicsSystem';

// ================================================================================================
export interface ILevelClass {
  new (game: Phaser.Game, parent?: Phaser.Group, name?: string, shouldAddToStage?: boolean,
    isBodyEnabled?: boolean, physicsBodyType?: number): Phaser.Group;
}

export interface ILevelCreatedData {
  bricks?: Brick[];
  borders?: Entity[];
}

interface ILevelElements {
  fBackground: Phaser.Sprite;
  fBordersGroup: Phaser.Group;
  fBricksGroup: Phaser.Group;
}

export default class LevelsSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _game: Game;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _backgroundGroup: Phaser.Group;

  private _physicsSystem: PhysicsSystem;

  private _audio: AudioPlayer;

  private _viewOffset: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, viewOffset: number, entitiesGroup: Phaser.Group, uiGroup: Phaser.Group,
    backgroundGroup: Phaser.Group, physicsSystem: PhysicsSystem, audio: AudioPlayer) {
    this._game = game;
    this._viewOffset = viewOffset;
    this._entitiesGroup = entitiesGroup;
    this._uiGroup = uiGroup;
    this._backgroundGroup = backgroundGroup;
    this._physicsSystem = physicsSystem;
    this._audio = audio;
  }

  // ----------------------------------------------------------------------------------------------
  build(leveClass: ILevelClass): ILevelCreatedData {
    const levelGroup = new leveClass(this._game, null, null, false);
    const levelElements = <ILevelElements> <any> levelGroup;

    const createdData: ILevelCreatedData = {};

    this.buildBackground(levelGroup, levelElements);
    createdData.borders = this.buildBorders(levelGroup, levelElements);
    createdData.bricks = this.buildBricks(levelGroup, levelElements);

    return (createdData);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private buildBackground(_levelGroup: Phaser.Group, elements: ILevelElements): void {
    this._backgroundGroup.add(elements.fBackground);
    this._backgroundGroup.y = this._viewOffset;
    elements.fBackground.width = this._game.width;
    elements.fBackground.height = this._game.height;
  }

  // ----------------------------------------------------------------------------------------------
  private buildBorders(_group: Phaser.Group, elements: ILevelElements): Entity[] {
    const borders: Entity[] = [];

    elements.fBordersGroup.forEach((unit: Phaser.Sprite) => {
      const border = new Entity(this._game, unit.x, unit.y, <string> unit.key, unit.frame,
        this._entitiesGroup, true);

      border.name = unit.name;
      border.alpha = unit.alpha;
      border.anchor.copyFrom(unit.anchor);
      border.pivot.set(unit.pivot.x, unit.pivot.y);
      border.rotation = unit.rotation;
      border.scale.copyFrom(unit.scale);

      border.physicsBody.setRectangle(border.width, border.height, border.width / 2,
        border.height / 2, border.rotation);

      borders.push(border);
    });

    this._physicsSystem.setWallsCollisionData(borders);

    return (borders);
  }

  // ----------------------------------------------------------------------------------------------
  private buildBricks(_group: Phaser.Group, elements: ILevelElements): Brick[] {
    const bricks: Brick[] = [];

    elements.fBricksGroup.forEach((unit: Phaser.Group) => {
      const image = <Phaser.Image> unit.getByName('image');
      const brick = new Brick(this._game, unit.x, unit.y, <string> image.key, <string> image.frame,
        this._entitiesGroup, <IBrickData> image.data, this._audio);

      brick.alpha = unit.alpha;
      brick.anchor.copyFrom(image.anchor);
      brick.pivot.set(unit.pivot.x, unit.pivot.y);

      const node = <Phaser.Image> unit.getByName('node');
      brick.physicsBody.setRectangle(
        node.width * unit.scale.x,
        node.height * unit.scale.y,
        node.x / node.width * unit.scale.x,
        node.y / node.height * unit.scale.y,
      );

      brick.physicsBody.rotation = unit.rotation;
      brick.scale.copyFrom(unit.scale);

      bricks.push(brick);
    });

    this._physicsSystem.setBricksCollisionData(bricks);

    return (bricks);
  }
}