import { Game } from '../../game';
import { addPoints, getPoints } from '../../util/util';

export default class ScoringSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _game: Game;

  private _numActiveBalls: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    this._game = game;

    this._numActiveBalls = 0;
  }

  // ----------------------------------------------------------------------------------------------
  brickDeff(): void {
    const config = this._game.shardsConfig;

    const multiBallsBonusPoints = Math.max(
      0,
      config.multiBallPointsMultiplier * (this._numActiveBalls - 1),
    );

    const finalPoints = config.pointsPerBrick + config.pointsPerBrick * multiBallsBonusPoints;
    addPoints(this._game, Math.floor(finalPoints));
  }

  // ----------------------------------------------------------------------------------------------
  lostBall(): void {
    let offset: number;

    const pointsLost = this._game.shardsConfig.pointsLost;
    if (typeof(pointsLost) === 'number') {
      offset = -pointsLost;
    } else {
      const percentage = Phaser.Math.clamp(Number.parseFloat(pointsLost), 0.0, 1.0);
      offset = -getPoints(this._game) * percentage;
    }

    addPoints(this._game, Math.floor(offset));
  }

  // ----------------------------------------------------------------------------------------------
  get numActiveBalls(): number {
    return (this._numActiveBalls);
  }

  // ----------------------------------------------------------------------------------------------
  set numActiveBalls(value: number) {
    this._numActiveBalls = value;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
}
