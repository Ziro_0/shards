import Ball from '../entities/Ball';
import Brick from '../entities/Brick';

// ================================================================================================
export interface IQuadTreeRect {
  x?: number;
  y?: number;
  right?: number;
  bottom?: number;
}

export default class BricksQuadTreeSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _bricksByRect: Map<IQuadTreeRect, Brick>;

  private _rectsByBrick: Map<Brick, IQuadTreeRect>;

  private _game: Phaser.Game;

  private _quadTree: Phaser.QuadTree;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, bricks?: Brick[]) {
    this._game = game;

    this._bricksByRect = new Map();
    this._rectsByBrick = new Map();

    this._quadTree = new Phaser.QuadTree(0, 0, this._game.world.width, this._game.world.height);
    this.addBricks(bricks);
  }

  // ----------------------------------------------------------------------------------------------
  addBricks(bricks: Brick[]): void {
    if (!bricks) {
      return;
    }

    bricks.forEach((brick) => {
      const rect = brick.getBounds();

      this._bricksByRect.set(rect, brick);
      this._rectsByBrick.set(brick, rect);

      this._quadTree.insert(rect);
    });
  }

  // ----------------------------------------------------------------------------------------------
  check(ball: Ball): Map<IQuadTreeRect, Brick> {
    const results: Map<IQuadTreeRect, Brick> = new Map();

    const rects: IQuadTreeRect[] = this._quadTree.retrieve(ball);

    rects.forEach((rect) => {
      results.set(rect, this._bricksByRect.get(rect))
    });

    return (results);
  }

  // ----------------------------------------------------------------------------------------------
  /**
   * Removes bricks from the quad tree.
   * 
   * @param bricks `Bricks[]` - The bricks to remove.
   * 
   * Note: The tree must be rebuilt for each removal.
   * 
   * @see https://github.com/photonstorm/phaser/issues/89
   */
  removeBricks(bricks: Brick[]): void {
    if (!bricks) {
      return;
    }

    if (this.removeBricksFromMaps(bricks) > 0) {
      this._quadTree.clear();
      this.addBricks(Array.from(this._bricksByRect.values()));
    }
  }

  // ----------------------------------------------------------------------------------------------
  render(): void {
    this._game.debug.quadTree(this._quadTree);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private removeBricksFromMaps(bricks: Brick[]): number {
    let numRemoved = 0;

    bricks.forEach((brick) => {
      const rect = this._rectsByBrick.get(brick);
      if (rect) {
        this._rectsByBrick.delete(brick);
        this._bricksByRect.delete(rect);
        numRemoved += 1;
      }
    });

    return (numRemoved);
  }
}
