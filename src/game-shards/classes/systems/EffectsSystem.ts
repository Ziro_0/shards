import { Game } from '../../game';
import Listeners from '../../Listeners';
import { isValidNumber, listenerCallback } from '../../util/util';
import AudioPlayer from '../audio/AudioPlayer';
import Ball from '../entities/Ball';
import { EffectIds } from '../entities/data/effectUtils';
import Item from '../entities/Item';
import Paddle, { PADDLE_SIZE } from '../entities/Paddle';
import PhysicsSystem from './PhysicsSystem';

// ================================================================================================
type ActivateFunction = (isStarting: boolean) => void;

interface IEffectData {
  activateFunction: ActivateFunction;
  timerEvent?: Phaser.TimerEvent;
  durationMs?: number;
}

// ================================================================================================

export default class EffectsSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _effectsData: Map<EffectIds, IEffectData>;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _game: Game;

  private _paddle: Paddle;

  private _audio: AudioPlayer;

  private _physicsSystem: PhysicsSystem;

  private _timer: Phaser.Timer;

  private _onBallsAdded: Phaser.Signal;

  private _viewOffset: number;

  private _noSoundsEnabled: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, viewOffset: number, entitiesGroup: Phaser.Group, uiGroup: Phaser.Group,
    audio: AudioPlayer, physicsSystem: PhysicsSystem) {
    this._onBallsAdded = new Phaser.Signal();

    this._game = game;
    this._viewOffset = viewOffset;
    this._entitiesGroup = entitiesGroup;
    this._uiGroup = uiGroup;
    this._audio = audio;
    this._physicsSystem = physicsSystem;

    this._timer = this._game.time.create(false);
    this._timer.start();

    this.initActivateEffects();
  }

  // ----------------------------------------------------------------------------------------------
  activate(effectId: EffectIds): void {
    const effectData = this._effectsData.get(effectId);
    if (!effectData) {
      console.warn('No effect data found with effect id:', effectId);
      return;
    }

    if (effectData.activateFunction) {
      const IS_STARTING = true;
      effectData.activateFunction.call(this, IS_STARTING);
    }

    if (isValidNumber(effectData.durationMs)) {
      this.startEffectsTimer(effectData);
    }
  }

  // ----------------------------------------------------------------------------------------------
  deactivate(effectId: EffectIds): void {
    const effectData = this._effectsData.get(effectId);
    if (!effectData) {
      console.warn('No effect data found with effect id:', effectId);
      return;
    }

    this.stopEffectsTimer(effectData);

    const IS_STARTING = false;
    effectData.activateFunction.call(this, IS_STARTING);
  }

  // ----------------------------------------------------------------------------------------------
  deactivateAll(): void {
    this._noSoundsEnabled = true;

    this._effectsData.forEach((_data, id) => {
      this.deactivate(id);
    });

    this._noSoundsEnabled = false;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this._timer.destroy();
    this._effectsData.clear();
    this._onBallsAdded.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  get onBallsAdded(): Phaser.Signal {
    return (this._onBallsAdded);
  }

  // ----------------------------------------------------------------------------------------------
  get paddle(): Paddle {
    return (this._paddle);
  }

  // ----------------------------------------------------------------------------------------------
  set paddle(value: Paddle) {
    this._paddle = value;
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(wasPrevWaveSuccessful?: boolean): void {
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private activateBomb(isStarting: boolean): void {
    if (isStarting) {
      this.destroyAllBalls();
      this.destroyAllItems();
      this.deactivateAll();
      this.paddle.explode();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private activateCannons(isStarting: boolean): void {
    if (isStarting) {
      this.playSound('snd_cannon_on');
      this.paddle.presentCannons();
    } else {
      this.playSound('snd_cannon_off');
      this._paddle.unpresentCannons();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private activateFastBalls(isStarting: boolean): void {
    if (isStarting) {
      this.playSound('snd_swoosh');
    }

    this._paddle.isFast = isStarting;

    this.getAllBalls().forEach((ball) => {
      ball.isFast = isStarting;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private activateGrow(isStarting: boolean): void {
    if (isStarting) {
      this.playSound('snd_paddle_enlarge');
      this._paddle.presentSize(PADDLE_SIZE.LARGE);
    } else {
      this.playSound('snd_paddle_shrink');
      this.paddle.presentSize(PADDLE_SIZE.NORMAL);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private activatePower(isStarting: boolean): void {
    if (isStarting) {
      this.playSound('snd_powerball');
    }

    this._paddle.isPowerful = isStarting;

    this.getAllBalls().forEach((ball) => {
      ball.isPowerful = isStarting;
      this._physicsSystem.setBallCollisionData(ball);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private activateShrink(isStarting: boolean): void {
    if (isStarting) {
      this.playSound('snd_paddle_shrink');
      this._paddle.presentSize(PADDLE_SIZE.SMALL);
    } else {
      this.playSound('snd_paddle_enlarge');
      this.paddle.presentSize(PADDLE_SIZE.NORMAL);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private activateSticky(isStarting: boolean): void {
    if (isStarting) {
      this._audio.play('snd_ball_sticky');
    }

    this._paddle.isSticky = isStarting;

    this.getAllBalls().forEach((ball) => {
      ball.isSticky = isStarting;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private activateTime(isStarting: boolean): void {
    if (!isStarting) {
      return;
    }

    this._audio.play('snd_life');
    listenerCallback(this._game, Listeners.SET_SECONDS, this._game.shardsConfig.timeBonusSeconds,
      true);
  }

  // ----------------------------------------------------------------------------------------------
  private activateTriple(isStarting: boolean): void {
    if (!isStarting) {
      return;
    }

    this._audio.play('snd_split_ball');

    const currentBalls = this.getAllBalls();
    let currentCount = currentBalls.length;
    const MAX_BALLS_ALLOWED = this._game.shardsConfig.maxBalls;
    if (currentCount >= MAX_BALLS_ALLOWED) {
      // cant create any more balls
      return;
    }

    const newBalls: Ball[] = [];

    const MAX_NUM_TO_CREATE_PER_BALL = 2;
    for (
      let spawnIndex = 0;
      spawnIndex < MAX_NUM_TO_CREATE_PER_BALL &&
      currentCount < MAX_BALLS_ALLOWED;
      spawnIndex += 1
    ) {
      for (
        let currentBallIndex = 0;
        currentBallIndex < currentBalls.length &&
        currentCount < MAX_BALLS_ALLOWED;
        currentBallIndex += 1
      ) {
        const currentBall = currentBalls[currentBallIndex];
        newBalls.push(this.spawnNewBall(currentBall, spawnIndex));
        currentCount += 1;
      };

      spawnIndex += 1;
    }

    this._onBallsAdded.dispatch(newBalls, this);
  }

  // ----------------------------------------------------------------------------------------------
  private destroyAllBalls(): void {
    const balls = this.getAllBalls();

    let ball = balls.pop();
    while (ball) {
      ball.destroy();
      ball = balls.pop();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private destroyAllItems(): void {
    const items: Item[] = [];

    this._entitiesGroup.forEach((gameObject: PIXI.DisplayObject) => {
      if (gameObject instanceof Item) {
        items.push(gameObject);
      }
    });

    items.forEach((item) => {
      item.destroy();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private getAllBalls(): Ball[] {
    const balls: Ball[] = [];

    this._entitiesGroup.forEach((gameObject: PIXI.DisplayObject) => {
      if (gameObject instanceof Ball) {
        balls.push(gameObject);
      }
    });

    return (balls);
  }

  // ----------------------------------------------------------------------------------------------
  private initActivateEffects() {
    const config = this._game.shardsConfig;

    const mapping: [EffectIds, IEffectData][] = [
      [
        EffectIds.BOMB,
        {
          activateFunction: this.activateBomb,
        },
      ],

      [
        EffectIds.CANNON,
        {
          activateFunction: this.activateCannons,
          durationMs: config.cannonsDurationMs,
        },
      ],

      [
        EffectIds.FAST,
        {
          activateFunction: this.activateFastBalls,
          durationMs: config.fastBallsDurationMs,
        },
      ],

      [
        EffectIds.GROW,
        {
          activateFunction: this.activateGrow,
          durationMs: config.sizeDurationMs,
        },
      ],

      [
        EffectIds.POWER,
        {
          activateFunction: this.activatePower,
          durationMs: config.powerBallDurationMs,
        },
      ],

      [
        EffectIds.SHRINK,
        {
          activateFunction: this.activateShrink,
          durationMs: config.sizeDurationMs,
        },
      ],

      [
        EffectIds.STICKY,
        {
          activateFunction: this.activateSticky,
          durationMs: config.stickyDurationMs,
        },
      ],

      [
        EffectIds.TIME,
        {
          activateFunction: this.activateTime,
        },
      ],

      [
        EffectIds.TRIPLE,
        {
          activateFunction: this.activateTriple,
        },
      ],
    ];

    this._effectsData = new Map(mapping);
  }

  // ----------------------------------------------------------------------------------------------
  private playSound(soundKeys: string | string[]): void {
    if (this._audio && !this._noSoundsEnabled) {
      this._audio.play(soundKeys);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private spawnNewBall(ball: Ball, spawnIndex: number): Ball {
    let radians = Phaser.Math.angleBetween(0, 0, ball.physicsBody.velocity.x,
      ball.physicsBody.velocity.y);
    const RADIANS_OFFSET = 2 * Math.PI / 3;
    radians += (RADIANS_OFFSET * (spawnIndex + 1));

    const DISTANCE = ball.width;

    const x = ball.x + Math.cos(radians) * DISTANCE;
    const y = ball.y - Math.sin(radians) * DISTANCE;
    const newBall = new Ball(this._game, x, y, this._entitiesGroup, this._audio,
      ball.quadTreeSystem);

    newBall.fire(radians);
    return (newBall);
  }

  // ----------------------------------------------------------------------------------------------
  private startEffectsTimer(effectData: IEffectData): void {
    this.stopEffectsTimer(effectData);

    effectData.timerEvent = this._timer.add(
      effectData.durationMs,

      () => {
        effectData.timerEvent = null;

        const IS_STARTING = false;
        effectData.activateFunction.call(this, IS_STARTING);
      },
    );
  }

  // ----------------------------------------------------------------------------------------------
  private stopEffectsTimer(effectData: IEffectData): void {
    if (effectData.timerEvent) {
      this._timer.remove(effectData.timerEvent);
      effectData.timerEvent = null;
    }
  }
}
