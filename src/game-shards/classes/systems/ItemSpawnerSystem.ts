import { Game } from '../../game';
import { chooseWeights } from '../../util/util';
import { EffectIds, getEffectIds } from '../entities/data/effectUtils';
import Item from '../entities/Item';
import PhysicsSystem from './PhysicsSystem';

export default class ItemSpawnerSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _game: Game;

  private _physicsSystem: PhysicsSystem;

  private _entitiesGroup: Phaser.Group;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, entitiesGroup: Phaser.Group, physicsSystem: PhysicsSystem) {
    this._game = game;
    this._entitiesGroup = entitiesGroup;
    this._physicsSystem = physicsSystem;
  }

  // ----------------------------------------------------------------------------------------------
  spawn(x: number, y: number): Item {
    if (Math.random() >= this._game.shardsConfig.itemDrops.chance) {
      return (null);
    }

    return (this.createItem(x, y));
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createItem(x: number, y: number): Item {
    const effectId = this.getRandomEffectId();
    const item = new Item(this._game, x, y, effectId, this._entitiesGroup);
    this._physicsSystem.setItemCollisionData(item);
    return (item);
  }

  // ----------------------------------------------------------------------------------------------
  private getRandomEffectId(): EffectIds {
    const weights = this._game.shardsConfig.itemDrops.weights;

    const weightValues: number[] = [];

    const EFFECT_IDS = getEffectIds();
    for (let index = 0; index < EFFECT_IDS.length; index += 1) {
      const effectId = EFFECT_IDS[index];
      weightValues.push(weights[effectId]);
    }

    const chosenIndex = chooseWeights(weightValues);
    return (EFFECT_IDS[chosenIndex]);
  }
}
