import { Game } from '../../game';
import Ball from '../entities/Ball';
import Brick from '../entities/Brick';
import Entity from '../entities/Entity';
import Item from '../entities/Item';
import Missile from '../entities/Missile';
import Paddle from '../entities/Paddle';

export default class PhysicsSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static DebugBodies: boolean;

  private _game: Game;

  private _cbmPoint: Phaser.Point;

  private _bricksCollisionGroup: Phaser.Physics.P2.CollisionGroup;

  private _entitiesCollisionGroup: Phaser.Physics.P2.CollisionGroup;

  private _itemsCollisionGroup: Phaser.Physics.P2.CollisionGroup;

  private _paddleCollisionGroup: Phaser.Physics.P2.CollisionGroup;

  private _wallsCollisionGroup: Phaser.Physics.P2.CollisionGroup;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, shouldCreate = true) {
    this._game = game;

    if (shouldCreate) {
      this.create();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get bricksCollisionGroup(): Phaser.Physics.P2.CollisionGroup {
    return (this._bricksCollisionGroup);
  }

  // ----------------------------------------------------------------------------------------------
  calcBallMagnitude(ball: Ball): number {
    if (!ball) {
      return (0);
    }

    const v = ball.physicsBody.velocity;
    this._cbmPoint.set(v.x, v.y);
    return(this._cbmPoint.getMagnitude());
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    const game = this._game;
    const config = game.shardsConfig;

    PhysicsSystem.DebugBodies = config.debugShowPhysicsBodies;

    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.restitution = 1.0;

    this.createWorldBounds();
    this.createCollisionGroups();

    this._cbmPoint = new Phaser.Point();
  }

  // ----------------------------------------------------------------------------------------------
  get entitiesCollisionGroup(): Phaser.Physics.P2.CollisionGroup {
    return (this._entitiesCollisionGroup);
  }

  // ----------------------------------------------------------------------------------------------
  interpolateBallMagnitude(ball: Ball, min: number, max: number): number {
    const MIN_MAG_THRESHOLD = 500;
    const MAX_MAG_THRESHOLD = 1500;
    const mag = this.calcBallMagnitude(ball);
    const ratio = (mag - MIN_MAG_THRESHOLD) / (MAX_MAG_THRESHOLD - MIN_MAG_THRESHOLD);
    const clampedRatio = Phaser.Math.clamp(ratio, 0, 1);
    return (Phaser.Math.linear(min, max, clampedRatio));
  }

  // ----------------------------------------------------------------------------------------------
  loadPolygon (body: Phaser.Physics.P2.Body, key: string, object?: any, scale?: Phaser.Point) {
    let data: any;

    if (key === null) {
      data = object;
    } else {
      data = this._game.cache.getPhysicsData(key, object);
    }

    if (!scale) {
      scale = new Phaser.Point(1.0, 1.0);
    }

    //  We've multiple Convex shapes, they should be CCW automatically
    const cm = p2.vec2.create();

    for (let i = 0; i < data.length; i += 1) {
      var vertices = [];

      for (let s = 0; s < data[i].shape.length; s += 2) {
        vertices.push([
          body.world.pxmi(data[i].shape[s] * scale.x),
          body.world.pxmi(data[i].shape[s + 1] * scale.y)
        ]);
      }

      const c = new p2.Convex({ vertices: vertices });

      // Move all vertices so its center of mass is in the local center of the convex
      for (let j = 0; j !== c.vertices.length; j += 1) {
        const v = c.vertices[j];
        p2.vec2.sub(v, v, c.centerOfMass);
      }

      p2.vec2.scale(cm, c.centerOfMass, 1);

      const origSpriteWidth = body.sprite.width / body.sprite.scale.x;
      cm[0] -= body.world.pxmi(origSpriteWidth / 2 * scale.x);

      const origSpriteHeight = body.sprite.height / body.sprite.scale.y;
      cm[1] -= body.world.pxmi(origSpriteHeight / 2 * scale.y);

      const UPD_TRI = 'updateTriangles';
      c[UPD_TRI]();

      c.updateCenterOfMass();
      c.updateBoundingRadius();

      body.data.addShape(c, cm);
    }

    body.data.aabbNeedsUpdate = true;
    body.shapeChanged();

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  get paddleCollisionGroup(): Phaser.Physics.P2.CollisionGroup {
    return (this._paddleCollisionGroup);
  }

  // ----------------------------------------------------------------------------------------------
  setBallCollisionData(ball: Ball): void {
    if (!ball) {
      return;
    }

    ball.physicsBody.clearCollision();

    ball.physicsBody.setCollisionGroup(this._entitiesCollisionGroup);

    const groups = [
      this._entitiesCollisionGroup,
      this._paddleCollisionGroup,
      this._wallsCollisionGroup,
    ];

    if (!ball.isPowerful) {
      groups.push(this._bricksCollisionGroup);
    }

    ball.physicsBody.collides(groups);
  }

  // ----------------------------------------------------------------------------------------------
  setBricksCollisionData(bricks: Brick[]): void {
    const _bricks = bricks || [];

    _bricks.forEach((brick) => {
      if (brick) {
        brick.physicsBody.setCollisionGroup(this._bricksCollisionGroup);
        brick.physicsBody.collides(this._entitiesCollisionGroup);
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  setItemCollisionData(item: Item): void {
    if (item) {
      item.physicsBody.setCollisionGroup(this._itemsCollisionGroup);
      item.physicsBody.collides(this._paddleCollisionGroup);
    }
  }

  // ----------------------------------------------------------------------------------------------
  setMissileCollisionData(missile: Missile): void {
    if (missile) {
      missile.physicsBody.setCollisionGroup(this._entitiesCollisionGroup);
      missile.physicsBody.collides([
        this._bricksCollisionGroup,
        this._wallsCollisionGroup,
      ]);
    }
  }

  // ----------------------------------------------------------------------------------------------
  setPaddleCollisionData(paddle: Paddle): void {
    if (paddle) {
      paddle.physicsBody.setCollisionGroup(this._paddleCollisionGroup);
      paddle.physicsBody.collides([
        this._entitiesCollisionGroup,
        this._itemsCollisionGroup,
      ]);
    }
  }

  // ----------------------------------------------------------------------------------------------
  setWallsCollisionData(borders: Entity[]): void {
    const _borders = borders || [];

    _borders.forEach((border) => {
      if (border) {
        border.physicsBody.setCollisionGroup(this._wallsCollisionGroup);
        border.physicsBody.collides([
          this._entitiesCollisionGroup,
        ]);
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  get wallsCollisionGroup(): Phaser.Physics.P2.CollisionGroup {
    return (this._wallsCollisionGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createCollisionGroups(): void {
    const p2 = this._game.physics.p2;

    this._bricksCollisionGroup = p2.createCollisionGroup();
    this._entitiesCollisionGroup = p2.createCollisionGroup();
    this._itemsCollisionGroup = p2.createCollisionGroup();
    this._paddleCollisionGroup = p2.createCollisionGroup();
    this._wallsCollisionGroup = p2.createCollisionGroup();

    p2.updateBoundsCollisionGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private createWorldBounds(): void {
    const IS_LEFT = true;
    const IS_RIGHT = true;
    const IS_TOP = true;
    const IS_BOTTOM = false;
    this._game.physics.p2.setBoundsToWorld(IS_LEFT, IS_RIGHT, IS_TOP, IS_BOTTOM);
  }
}
