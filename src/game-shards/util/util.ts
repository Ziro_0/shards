import { IPointsResult } from '../../pages/home/home';
import { Game } from '../game';
import Listeners from '../Listeners';

// ================================================================================================
// CONSTS, INTERFACES, TYPES
// ================================================================================================

/**
 * Defines a 2D point.
 */
 export interface IPoint {
  /**
   * The x coordinate of the point.
   */
  x?: number;
  
  /**
   * The y coordinate of the point.
   */
  y?: number;
}

// ------------------------------------------------------------------------------------------------
/**
 * Defines a 2D rectangle.
 */
 export interface IRect extends IPoint {
  /**
   * The width of the rectangle.
   */
  width?: number;
  
  /**
   * The height of the rectangle.
   */
  height?: number;
}

// ================================================================================================
// FUNCTIONS
// ================================================================================================

// ----------------------------------------------------------------------------------------------
export function addPoints(game: Game, points: number): void {
  listenerCallback(game, Listeners.ADD_POINT, points, true);
}

// ----------------------------------------------------------------------------------------------
export function choose(...items: any[]): any {
  if (items.length === 0) {
    throw new Error('You need to pass at least one item');
  }
  return (items[irandom(items.length - 1)]);
}

// ------------------------------------------------------------------------------------------------
export function chooseWeights(weights: number[]): number {
  const length = weights ? weights.length : 0;
  if (length === 0) {
    return (-1);
  }

  let sumOfWeights = 0;
  weights.forEach((weight) => {
    sumOfWeights += weight;
  });

  const threshold = Math.random() * sumOfWeights;
  let test = 0;
  for (let index = 0; index < length - 1; index += 1) {
    test += weights[index];
    if (test >= threshold) {
      return (index);
    }
  }

  return (length - 1); //random number was in range of the last index
}

// ------------------------------------------------------------------------------------------------
export function checkOverlap(spriteA: PIXI.Sprite, spriteB: PIXI.Sprite): boolean {
  const boundsA = new Phaser.Rectangle().copyFromBounds(spriteA.getBounds());
  const boundsB = new Phaser.Rectangle().copyFromBounds(spriteB.getBounds());
  return (Phaser.Rectangle.intersects(boundsA, boundsB));
}

// ------------------------------------------------------------------------------------------------
export function copyObject(inObject: any): any {
  if (typeof inObject !== 'object' || inObject === null || inObject === undefined) {
    //  inObject is not an object
    return (inObject);
  }

  // Create an array or object to hold the values
  const outObject = Array.isArray(inObject) ? [] : {};

  for (let key in inObject) {
    const value = inObject[key];

    //  Recursively (deep) copy for nested objects, including arrays
    outObject[key] = copyObject(value);
  }

  return (outObject);
}

// ------------------------------------------------------------------------------------------------
export function getChildByName(container: PIXI.DisplayObjectContainer,
  name: string): PIXI.DisplayObject {
  if (!container) {
    return (null);
  }

  return (container.children.find((child) => 'name' in child && child['name'] === name));
}

// ------------------------------------------------------------------------------------------------
export function getP2PhysicsBodyOf(sprite: Phaser.Sprite): Phaser.Physics.P2.Body {
  return (sprite ? sprite.body : null);
}

// ------------------------------------------------------------------------------------------------
export function getProperty(source: object, name: string, defaultValue?: any): any {
  if (!source || !name) {
    return (defaultValue);
  }

  let keys = name.split('.');
  const len = keys.length;
  let i = 0;
  let val = source;

  while (i < len) {
    const key = keys[i];

    if (val !== null && val !== undefined) {
      val = val[key];
      i += 1;
    } else {
      return (defaultValue);
    }
  }

  return (val !== null && val !== undefined ? val : defaultValue);
}

// ------------------------------------------------------------------------------------------------
export function getBooleanProperty(source: object, name: string,
  defaultValue?: boolean): boolean {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function getIntProperty(source: object, name: string, defaultValue?: number): number {
  return (Math.floor(getNumberProperty(source, name, defaultValue)));
}

// ------------------------------------------------------------------------------------------------
export function getNumberProperty(source: object, name: string, defaultValue?: number): number {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function getPoints(game: Game): number {
  const result: IPointsResult = {};
  listenerCallback(game, Listeners.GET_POINTS, result);
  return (result.value)
}

// ------------------------------------------------------------------------------------------------
export function getStringProperty(source: object, name: string, defaultValue?: string): string {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function irandom(max: number): number {
  return (Math.floor(Math.random() * (max + 1)));
}

// ------------------------------------------------------------------------------------------------
export function irandomRange(min: number, max: number): number {
  return (Math.floor(Math.random() * (max - min + 1)) + min);
}

// ------------------------------------------------------------------------------------------------
export function isMobile(game: Phaser.Game): boolean {
  const device = game.device;
  return (device.android || device.iOS || device.windowsPhone || device.touch);
}

// ----------------------------------------------------------------------------------------------
export function isValidNumber(value: number): boolean {
  return (!Number.isNaN(value) && typeof (value) === 'number');
}

// ----------------------------------------------------------------------------------------------
export function listenerCallback(game: Game, key: string, ...args: any[]): void {
  const callback: Function = game.listenerMapping[key];
  if (callback) {
    callback.call(game, ...args);
  }
}

// ------------------------------------------------------------------------------------------------
export function randomRange(min: number, max: number): number {
  return (Math.random() * (max - min) + min);
}

// ------------------------------------------------------------------------------------------------
export function removeFromArray(array: any[], item: any): boolean {
  if (!array) {
    return (false);
  }

  const index = array.indexOf(item);
  if (index === -1) {
    return (false);
  }

  array.splice(index, 1);
  return (true);
}

// ------------------------------------------------------------------------------------------------
export function removeTimer(timerEvent: Phaser.TimerEvent): Phaser.TimerEvent {
  if (timerEvent) {
    timerEvent.timer.remove(timerEvent);
  }

  return (null);
}
